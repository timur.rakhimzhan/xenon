"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const seasons_1 = require("../database/models/seasons");
const sequelize_1 = require("sequelize");
class Season {
    constructor(sportName, leagueName, seasonName) {
        this.sports_name = sportName;
        this.leagues_name = leagueName;
        this.seasons_name = seasonName;
    }
    init() {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {};
            if (this.seasons_name) {
                options["name"] = this.seasons_name;
            }
            else {
                options["current"] = true;
            }
            const res = yield seasons_1.Seasons.findOne({
                where: options,
                include: [{
                        association: seasons_1.Seasons.Sports,
                        where: sequelize_1.Sequelize.where(sequelize_1.Sequelize.fn('lower', sequelize_1.Sequelize.col('Sport.name')), sequelize_1.Sequelize.fn('lower', this.sports_name)),
                    }, {
                        association: seasons_1.Seasons.Leagues,
                        where: sequelize_1.Sequelize.where(sequelize_1.Sequelize.fn('lower', sequelize_1.Sequelize.col('League.name')), sequelize_1.Sequelize.fn('lower', this.leagues_name))
                    }]
            });
            if (!res) {
                throw new Error(`No such season: ${this.seasons_name} on ${this.sports_name} ${this.leagues_name}`);
            }
            if (res) {
                this.sports_id = res.get("sports_id");
                this.leagues_id = res.get("leagues_id");
                this.id = res.get("id");
                this.start_date = res.get("start_date");
                this.end_date = res.get("end_date");
                this.sports_espn_name = res.get("Sport").get("espn_name");
                this.leagues_espn_name = res.get("League").get("espn_name");
                this.seasons_name = res.get("name");
                this.current = res.get("current");
            }
        });
    }
    get matchesTableName() {
        return `statistics_${this.sports_name.toLowerCase()}_${this.leagues_name.toLowerCase()}_${this.seasons_name}`;
    }
}
exports.Season = Season;
//# sourceMappingURL=season.js.map