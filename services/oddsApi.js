"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const SERVER = "http://localhost";
const PORT = 3000;
function findMatch(season) {
    const { sports_name, leagues_name } = season;
    return `${SERVER}:${PORT}/api/findMatch/${sports_name}/${leagues_name}/`;
}
exports.findMatch = findMatch;
function updateAbbrev(season) {
    const { sports_name, leagues_name } = season;
    return `${SERVER}:${PORT}/api/updateAbbrev/${sports_name}/${leagues_name}/`;
}
exports.updateAbbrev = updateAbbrev;
function updateDate(season) {
    const { sports_name, leagues_name } = season;
    return `${SERVER}:${PORT}/api/updateDate/${sports_name}/${leagues_name}/`;
}
exports.updateDate = updateDate;
//# sourceMappingURL=oddsApi.js.map