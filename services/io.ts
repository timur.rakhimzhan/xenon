import * as fs from 'fs';

export function getFile<T extends object> (filepath: string): Promise<T> {
    return new Promise<T>((resolve, reject) => {
        fs.readFile(filepath, (err, data)=> {
            resolve(JSON.parse(data.toString()));
        });
    })
}

export function appendToFile(filepath: string, text: string): Promise<void> {
    return new Promise(async (resolve, reject) => {
        fs.appendFile(filepath, text, {}, (err) => {
            console.log('Appended.');
            resolve();
        });
    });
}

export function writeFile (filepath: string, data: object | string): Promise<string> {
    if(typeof data === 'object')
        data = JSON.stringify(data);
    return new Promise((resolve, reject) => {
        fs.writeFile(filepath, data as string, (err) => {
            if(err)
                reject(err);
            resolve(filepath);
        })
    })
}
