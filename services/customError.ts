export const INVALID_EVENT_ERROR = "INVALID_EVENT_ERROR";
export const EVENT_NOT_FOUND_ERROR = "EVENT_NOT_FOUND_ERROR";
export const ESPN_SERVER_ERROR = "ESPN_SERVER_ERROR";
export const MORE_THAN_ONE_MATCH = "MORE_THAN_ONE_MATCH"

export function acceptableError<T extends Error>(error: CustomError<T>) {
    const set = new Set([
        INVALID_EVENT_ERROR,
        ESPN_SERVER_ERROR,
        EVENT_NOT_FOUND_ERROR,
        MORE_THAN_ONE_MATCH
    ]);
    return set.has(error.type);

}



export class CustomError<T extends Error> extends Error {
    public type: string;
    public originalError?: T;

    constructor(message: string, type: string, originalError?: T) {
        super(message);
        this.type = type;
        if(originalError) {
            this.originalError = originalError;
        }
    }

}