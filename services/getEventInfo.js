"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const espnApi_1 = require("./espnApi");
const helpers_1 = require("../tablemaker/helpers");
const fetchCoeff_1 = require("../tablemaker/databaseRequests/fetchCoeff");
const customError_1 = require("./customError");
function getEventInfo(season, eventScoreboard) {
    var _a, _b, _c, _d;
    return __awaiter(this, void 0, void 0, function* () {
        let match_espn;
        try {
            match_espn = (yield axios_1.default.get(espnApi_1.espnMatchById(season, eventScoreboard.id), { timeout: 10000 })).data;
        }
        catch (error) {
            if (((_b = (_a = error) === null || _a === void 0 ? void 0 : _a.response) === null || _b === void 0 ? void 0 : _b.status) === 404) {
                throw new customError_1.CustomError(`Error not found: ${eventScoreboard.id}`, customError_1.EVENT_NOT_FOUND_ERROR, error);
            }
            else if (((_d = (_c = error) === null || _c === void 0 ? void 0 : _c.response) === null || _d === void 0 ? void 0 : _d.status) === 502) {
                throw new customError_1.CustomError(`Server error while fetching event: ${eventScoreboard.id}`, customError_1.ESPN_SERVER_ERROR, error);
            }
            else {
                throw error;
            }
        }
        const home_espn = eventScoreboard.competitions[0].competitors.find((competitor) => competitor.homeAway === "home");
        const away_espn = eventScoreboard.competitions[0].competitors.find((competitor) => competitor.homeAway === "away");
        if (!home_espn || !away_espn) {
            throw new customError_1.CustomError(`Error while event fetching competitor: ${eventScoreboard.id}`, customError_1.INVALID_EVENT_ERROR);
        }
        let home_competitor = match_espn.boxscore.teams.find(({ team }) => team.abbreviation === home_espn.team.abbreviation);
        let away_competitor = match_espn.boxscore.teams.find(({ team }) => team.abbreviation === away_espn.team.abbreviation);
        if (!home_competitor || !away_competitor) {
            throw new customError_1.CustomError(`Error while event fetching competitor: ${eventScoreboard.id}`, customError_1.INVALID_EVENT_ERROR);
        }
        const home_statistics = helpers_1.statisticsProcessor(home_competitor.statistics, "home_stats");
        const away_statistics = helpers_1.statisticsProcessor(away_competitor.statistics, "away_stats");
        const { home_prediction, away_prediction } = helpers_1.getEspnPrediction(match_espn);
        const { home_pickcenter, away_pickcenter } = helpers_1.getPickCenter(match_espn);
        const { home_coeff, away_coeff } = yield fetchCoeff_1.fetchCoeffs(season, eventScoreboard, home_espn, away_espn);
        const home = {
            name: home_espn.team.displayName,
            abbreviation: home_espn.team.abbreviation,
            espn_id: parseInt(home_espn.team.id)
        };
        const away = {
            name: away_espn.team.displayName,
            abbreviation: away_espn.team.abbreviation,
            espn_id: parseInt(away_espn.team.id)
        };
        const match = {
            neutral_site: eventScoreboard.competitions[0].neutralSite,
            espn_id: parseInt(eventScoreboard.id),
            start_date: eventScoreboard.competitions[0].startDate,
            home_score: home_espn.score ? parseInt(home_espn.score) : null,
            home_coeff,
            home_prediction,
            home_pickcenter,
            away_score: away_espn.score ? parseInt(away_espn.score) : null,
            away_coeff,
            away_prediction,
            away_pickcenter,
            overtime: eventScoreboard.status.type.altDetail === "OT",
            periods: eventScoreboard.status.period,
            clock: eventScoreboard.status.clock
        };
        return { match, home, away, home_statistics, away_statistics, status: eventScoreboard.status.type.name };
    });
}
exports.getEventInfo = getEventInfo;
//# sourceMappingURL=getEventInfo.js.map