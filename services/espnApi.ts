import {Season} from "./season";

export const ESPN_SERVER = "http://site.api.espn.com/apis/site/v2/sports";

export function espnScoreboardByDate(season: Season, startDate: string, endDate?: string): string {
    const {sports_espn_name, leagues_espn_name}: Season = season;
    if(endDate)
        return `${ESPN_SERVER}/${sports_espn_name?.toLowerCase()}/${leagues_espn_name?.toLowerCase()}/scoreboard?lang=en&dates=${startDate}-${endDate}`;
    return `${ESPN_SERVER}/${sports_espn_name?.toLowerCase()}/${leagues_espn_name?.toLowerCase()}/scoreboard?lang=en&dates=${startDate}`;
}

export function espnScoreboardById(season: Season, id: string): string {
    const {sports_espn_name, leagues_espn_name}: Season = season;
    return `${ESPN_SERVER}/${sports_espn_name?.toLowerCase()}/${leagues_espn_name?.toLowerCase()}/scoreboard/${id}`;
}


export function espnMatchById(season: Season, id: string): string {
    const {sports_espn_name, leagues_espn_name}: Season = season;
    return `${ESPN_SERVER}/${sports_espn_name?.toLowerCase()}/${leagues_espn_name?.toLowerCase()}/summary?lang=en&event=${id}`;
}

export function espnMatchCurrent(season: Season): string {
    const {sports_espn_name, leagues_espn_name}: Season = season;
    return `${ESPN_SERVER}/${sports_espn_name?.toLowerCase()}/${leagues_espn_name?.toLowerCase()}/scoreboard?lang=en&limit=1000`;
}