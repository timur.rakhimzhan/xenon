import {Season} from "./season";

const SERVER = "http://localhost";
const PORT = 3000;

export function findMatch(season: Season) {
    const {sports_name, leagues_name}: Season = season;
    return `${SERVER}:${PORT}/api/findMatch/${sports_name}/${leagues_name}/`
}

export function updateAbbrev(season: Season) {
    const {sports_name, leagues_name}: Season = season;
    return `${SERVER}:${PORT}/api/updateAbbrev/${sports_name}/${leagues_name}/`
}

export function updateDate(season: Season) {
    const {sports_name, leagues_name}: Season = season;
    return `${SERVER}:${PORT}/api/updateDate/${sports_name}/${leagues_name}/`
}