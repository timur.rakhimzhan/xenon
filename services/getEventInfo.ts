import {EspnCompetitorI, EspnScoreboardI, EspnMatchI} from "../typings/tablemaker/espn";
import axios from "axios";
import {espnMatchById} from "./espnApi";
import {MatchInfoDBI, StatisticsDBI, TeamInfoDBI, TeamStatsI} from "../typings/tablemaker/matchesDB";
import {getEspnPrediction, getPickCenter, statisticsProcessor} from "../tablemaker/helpers";
import {fetchCoeffs} from "../tablemaker/databaseRequests/fetchCoeff";
import {Season} from "./season";
import {CustomError, ESPN_SERVER_ERROR, EVENT_NOT_FOUND_ERROR, INVALID_EVENT_ERROR} from "./customError";

export async function getEventInfo(season: Season, eventScoreboard: EspnScoreboardI): Promise<StatisticsDBI> {
    let match_espn: EspnMatchI;
    try {
        match_espn = (await axios.get(espnMatchById(season, eventScoreboard.id), {timeout: 10000})).data;
    } catch (error) {
        if(error?.response?.status === 404) {
            throw new CustomError(`Error not found: ${eventScoreboard.id}`, EVENT_NOT_FOUND_ERROR, error)
        } else if(error?.response?.status === 502) {
            throw new CustomError(`Server error while fetching event: ${eventScoreboard.id}`, ESPN_SERVER_ERROR, error)
        } else {
            throw error;
        }
    }
    const home_espn: EspnCompetitorI | undefined = eventScoreboard.competitions[0].competitors.find((competitor: EspnCompetitorI) => competitor.homeAway === "home");
    const away_espn: EspnCompetitorI | undefined = eventScoreboard.competitions[0].competitors.find((competitor: EspnCompetitorI) => competitor.homeAway === "away");

    if(!home_espn || !away_espn) {
        throw new CustomError(`Error while event fetching competitor: ${eventScoreboard.id}`, INVALID_EVENT_ERROR);
    }

    let home_competitor: EspnCompetitorI | undefined = match_espn.boxscore.teams.find(({team}: EspnCompetitorI) => team.abbreviation === home_espn.team.abbreviation);
    let away_competitor: EspnCompetitorI | undefined = match_espn.boxscore.teams.find(({team}: EspnCompetitorI) => team.abbreviation === away_espn.team.abbreviation);

    if(!home_competitor || !away_competitor) {
        throw new CustomError(`Error while event fetching competitor: ${eventScoreboard.id}`, INVALID_EVENT_ERROR);
    }

    const home_statistics: TeamStatsI = statisticsProcessor(home_competitor.statistics, "home_stats");
    const away_statistics: TeamStatsI = statisticsProcessor(away_competitor.statistics, "away_stats");

    const {home_prediction, away_prediction} = getEspnPrediction(match_espn);
    const {home_pickcenter, away_pickcenter} = getPickCenter(match_espn);
    const {home_coeff, away_coeff} = await fetchCoeffs(season, eventScoreboard, home_espn, away_espn);
    const home: TeamInfoDBI = {
        name: home_espn.team.displayName,
        abbreviation: home_espn.team.abbreviation,
        espn_id: parseInt(home_espn.team.id)
    };
    const away: TeamInfoDBI = {
        name: away_espn.team.displayName,
        abbreviation: away_espn.team.abbreviation,
        espn_id: parseInt(away_espn.team.id)
    };
    const match: MatchInfoDBI = {
        neutral_site: eventScoreboard.competitions[0].neutralSite,
        espn_id: parseInt(eventScoreboard.id),
        start_date: eventScoreboard.competitions[0].startDate,
        home_score: home_espn.score ? parseInt(home_espn.score) : null,
        home_coeff,
        home_prediction,
        home_pickcenter,
        away_score: away_espn.score ? parseInt(away_espn.score): null,
        away_coeff,
        away_prediction,
        away_pickcenter,
        overtime: eventScoreboard.status.type.altDetail === "OT",
        periods: eventScoreboard.status.period,
        clock: eventScoreboard.status.clock
    };
    return {match, home, away, home_statistics, away_statistics, status: eventScoreboard.status.type.name};
}