import {Seasons} from "../database/models/seasons";
import {Model, Sequelize, WhereAttributeHash} from "sequelize";
import {ESPN_SERVER} from "./espnApi";

export class Season {
    public sports_name: string;
    public leagues_name: string;
    public seasons_name?: string;
    public start_date?: string;
    public end_date?: string;
    public sports_id?: number;
    public leagues_id?: number;
    public id?: number;
    public sports_espn_name?: string;
    public leagues_espn_name?: string;
    public current?: boolean;


    constructor(sportName: string, leagueName: string, seasonName?: string) {
        this.sports_name = sportName;
        this.leagues_name = leagueName;
        this.seasons_name = seasonName;
    }

    public async init() {
        const options: WhereAttributeHash = {};
        if(this.seasons_name) {
            options["name"] = this.seasons_name
        } else {
            options["current"] = true;
        }
        const res: Seasons | null = await Seasons.findOne({
            where: options,
            include: [{
                association: Seasons.Sports,
                where: Sequelize.where(Sequelize.fn('lower', Sequelize.col('Sport.name')), Sequelize.fn('lower', this.sports_name)),
            }, {
                association: Seasons.Leagues,
                where: Sequelize.where(Sequelize.fn('lower', Sequelize.col('League.name')), Sequelize.fn('lower', this.leagues_name))
            }]
        });
        if (!res) {
            throw new Error(`No such season: ${this.seasons_name} on ${this.sports_name} ${this.leagues_name}`);
        }
        if (res) {
            this.sports_id = res.get("sports_id") as number;
            this.leagues_id = res.get("leagues_id") as number;
            this.id = res.get("id") as number;
            this.start_date = res.get("start_date") as string;
            this.end_date = res.get("end_date") as string;
            this.sports_espn_name = (res.get("Sport") as Model).get("espn_name") as string;
            this.leagues_espn_name = (res.get("League") as Model).get("espn_name") as string;
            this.seasons_name = res.get("name") as string;
            this.current = res.get("current") as boolean;
        }
    }

    get matchesTableName(): string {
        return `statistics_${this.sports_name.toLowerCase()}_${this.leagues_name.toLowerCase()}_${this.seasons_name}`;
    }
}