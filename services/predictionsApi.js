"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const SERVER = "http://localhost";
const PORT = 5000;
function predictionsApi(season, modelConfigName, espn) {
    return `${SERVER}:${PORT}/${espn ? "espn" : "xenon"}/${season.sports_name}/${season.leagues_name}/${modelConfigName}/predict`;
}
exports.predictionsApi = predictionsApi;
//# sourceMappingURL=predictionsApi.js.map