"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.INVALID_EVENT_ERROR = "INVALID_EVENT_ERROR";
exports.EVENT_NOT_FOUND_ERROR = "EVENT_NOT_FOUND_ERROR";
exports.ESPN_SERVER_ERROR = "ESPN_SERVER_ERROR";
exports.MORE_THAN_ONE_MATCH = "MORE_THAN_ONE_MATCH";
function acceptableError(error) {
    const set = new Set([
        exports.INVALID_EVENT_ERROR,
        exports.ESPN_SERVER_ERROR,
        exports.EVENT_NOT_FOUND_ERROR,
        exports.MORE_THAN_ONE_MATCH
    ]);
    return set.has(error.type);
}
exports.acceptableError = acceptableError;
class CustomError extends Error {
    constructor(message, type, originalError) {
        super(message);
        this.type = type;
        if (originalError) {
            this.originalError = originalError;
        }
    }
}
exports.CustomError = CustomError;
//# sourceMappingURL=customError.js.map