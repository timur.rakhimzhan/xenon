import {Season} from "./season";

const SERVER = "http://localhost";
const PORT = 5000;

export function predictionsApi(season: Season, modelConfigName: string, espn: boolean) {
    return `${SERVER}:${PORT}/${espn ? "espn" : "xenon"}/${season.sports_name}/${season.leagues_name}/${modelConfigName}/predict`;
}