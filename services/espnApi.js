"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ESPN_SERVER = "http://site.api.espn.com/apis/site/v2/sports";
function espnScoreboardByDate(season, startDate, endDate) {
    var _a, _b, _c, _d;
    const { sports_espn_name, leagues_espn_name } = season;
    if (endDate)
        return `${exports.ESPN_SERVER}/${(_a = sports_espn_name) === null || _a === void 0 ? void 0 : _a.toLowerCase()}/${(_b = leagues_espn_name) === null || _b === void 0 ? void 0 : _b.toLowerCase()}/scoreboard?lang=en&dates=${startDate}-${endDate}`;
    return `${exports.ESPN_SERVER}/${(_c = sports_espn_name) === null || _c === void 0 ? void 0 : _c.toLowerCase()}/${(_d = leagues_espn_name) === null || _d === void 0 ? void 0 : _d.toLowerCase()}/scoreboard?lang=en&dates=${startDate}`;
}
exports.espnScoreboardByDate = espnScoreboardByDate;
function espnScoreboardById(season, id) {
    var _a, _b;
    const { sports_espn_name, leagues_espn_name } = season;
    return `${exports.ESPN_SERVER}/${(_a = sports_espn_name) === null || _a === void 0 ? void 0 : _a.toLowerCase()}/${(_b = leagues_espn_name) === null || _b === void 0 ? void 0 : _b.toLowerCase()}/scoreboard/${id}`;
}
exports.espnScoreboardById = espnScoreboardById;
function espnMatchById(season, id) {
    var _a, _b;
    const { sports_espn_name, leagues_espn_name } = season;
    return `${exports.ESPN_SERVER}/${(_a = sports_espn_name) === null || _a === void 0 ? void 0 : _a.toLowerCase()}/${(_b = leagues_espn_name) === null || _b === void 0 ? void 0 : _b.toLowerCase()}/summary?lang=en&event=${id}`;
}
exports.espnMatchById = espnMatchById;
function espnMatchCurrent(season) {
    var _a, _b;
    const { sports_espn_name, leagues_espn_name } = season;
    return `${exports.ESPN_SERVER}/${(_a = sports_espn_name) === null || _a === void 0 ? void 0 : _a.toLowerCase()}/${(_b = leagues_espn_name) === null || _b === void 0 ? void 0 : _b.toLowerCase()}/scoreboard?lang=en&limit=1000`;
}
exports.espnMatchCurrent = espnMatchCurrent;
//# sourceMappingURL=espnApi.js.map