import {CustomModel} from "./custom";
import { DataTypes, HasMany, Sequelize} from "sequelize";
import {Predictions} from "./predictions";

export class Odds extends CustomModel {
    static Predictions: HasMany<Odds, Predictions>
}

export function initOdds(sequelize: Sequelize) {
    Odds.init({
        name: {
            allowNull: false,
            unique: true,
            type: DataTypes.STRING
        }
    }, {sequelize, tableName: "odds"});
}

