import {CustomModel} from "./custom";
import {DataTypes, HasMany, Sequelize} from "sequelize";
import {ModelConfigs} from "./modelConfigs";

export class Models extends CustomModel {
    static ModelConfigs: HasMany<Models, ModelConfigs>;
}

export function initModels(sequelize: Sequelize) {
    Models.init({
        name: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        }
    }, {sequelize, tableName: "models"});
}
