"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const custom_1 = require("./custom");
const sequelize_1 = require("sequelize");
const models_1 = require("./models");
const sports_1 = require("./sports");
const leagues_1 = require("./leagues");
const seasons_1 = require("./seasons");
class ModelConfigs extends custom_1.CustomModel {
}
exports.ModelConfigs = ModelConfigs;
function initModelConfigs(sequelize) {
    ModelConfigs.init({
        name: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: false
        },
        columns: sequelize_1.DataTypes.ARRAY(sequelize_1.DataTypes.STRING),
        min_coeff: sequelize_1.DataTypes.FLOAT,
        max_coeff: sequelize_1.DataTypes.FLOAT,
        bookmaker_margin: sequelize_1.DataTypes.FLOAT,
        difference: sequelize_1.DataTypes.FLOAT,
        config: sequelize_1.DataTypes.JSONB,
        last_year_roi: sequelize_1.DataTypes.FLOAT,
        last_year_profit: sequelize_1.DataTypes.FLOAT,
        last_year_bets_number: sequelize_1.DataTypes.FLOAT,
        in_use: {
            type: sequelize_1.DataTypes.BOOLEAN,
            defaultValue: false,
        }
    }, { sequelize, tableName: "model_configs" });
    sports_1.Sports.ModelConfigs = sports_1.Sports.hasMany(ModelConfigs, { foreignKey: { name: "sports_id", allowNull: false }, sourceKey: "id" });
    ModelConfigs.Sports = ModelConfigs.belongsTo(sports_1.Sports, { foreignKey: { name: "sports_id", allowNull: false }, targetKey: "id" });
    leagues_1.Leagues.ModelConfigs = leagues_1.Leagues.hasMany(ModelConfigs, { foreignKey: { name: "leagues_id", allowNull: false }, sourceKey: "id" });
    ModelConfigs.Leagues = ModelConfigs.belongsTo(leagues_1.Leagues, { foreignKey: { name: "leagues_id", allowNull: false }, targetKey: "id" });
    models_1.Models.ModelConfigs = models_1.Models.hasMany(ModelConfigs, { foreignKey: { name: "models_id", allowNull: false }, sourceKey: "id" });
    ModelConfigs.Models = ModelConfigs.belongsTo(models_1.Models, { foreignKey: { name: "models_id", allowNull: false }, targetKey: "id" });
    seasons_1.Seasons.ModelConfigsTrain = seasons_1.Seasons.hasMany(ModelConfigs, { foreignKey: { name: "seasons_train_id", allowNull: true }, sourceKey: "id", as: "ModelConfigsTrain" });
    ModelConfigs.SeasonsTrain = ModelConfigs.belongsTo(seasons_1.Seasons, { foreignKey: { name: "seasons_train_id", allowNull: true }, targetKey: "id", as: "SeasonsTrain" });
}
exports.initModelConfigs = initModelConfigs;
//# sourceMappingURL=modelConfigs.js.map