"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const sports_1 = require("./sports");
const leagues_1 = require("./leagues");
const custom_1 = require("./custom");
class Seasons extends custom_1.CustomModel {
}
exports.Seasons = Seasons;
function initSeasons(sequelize) {
    Seasons.init({
        name: sequelize_1.DataTypes.STRING,
        start_date: {
            type: sequelize_1.DataTypes.STRING(10),
        },
        end_date: {
            type: sequelize_1.DataTypes.STRING(10),
        },
        current: {
            type: sequelize_1.DataTypes.BOOLEAN,
            defaultValue: false
        }
    }, { tableName: 'seasons', sequelize });
    sports_1.Sports.Seasons = sports_1.Sports.hasMany(Seasons, { foreignKey: { name: "sports_id", allowNull: false }, sourceKey: "id" });
    Seasons.Sports = Seasons.belongsTo(sports_1.Sports, { foreignKey: { name: "sports_id", allowNull: false }, targetKey: "id" });
    leagues_1.Leagues.Seasons = leagues_1.Leagues.hasMany(Seasons, { foreignKey: { name: "leagues_id", allowNull: false }, sourceKey: "id" });
    Seasons.Leagues = Seasons.belongsTo(leagues_1.Leagues, { foreignKey: { name: "leagues_id", allowNull: false }, targetKey: "id" });
    Seasons.PreviousSeason = Seasons.hasMany(Seasons, { foreignKey: { name: "previous_seasons_id", allowNull: true }, sourceKey: "id" });
}
exports.initSeasons = initSeasons;
//# sourceMappingURL=seasons.js.map