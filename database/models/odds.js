"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const custom_1 = require("./custom");
const sequelize_1 = require("sequelize");
class Odds extends custom_1.CustomModel {
}
exports.Odds = Odds;
function initOdds(sequelize) {
    Odds.init({
        name: {
            allowNull: false,
            unique: true,
            type: sequelize_1.DataTypes.STRING
        }
    }, { sequelize, tableName: "odds" });
}
exports.initOdds = initOdds;
//# sourceMappingURL=odds.js.map