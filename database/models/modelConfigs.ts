import {CustomModel} from "./custom";
import Model, {DataTypes, Sequelize, BelongsTo, HasMany} from "sequelize";
import {Models} from "./models";
import {Sports} from "./sports";
import {Leagues} from "./leagues";
import {Predictions} from "./predictions";
import {Seasons} from "./seasons";
import {Season} from "../../services/season";

export class ModelConfigs extends CustomModel {
    static Models: BelongsTo<ModelConfigs, Models>;
    static Sports: BelongsTo<ModelConfigs, Sports>;
    static Leagues: BelongsTo<ModelConfigs, Leagues>;
    static SeasonsTrain: BelongsTo<ModelConfigs, Seasons>;
    static Predictions: HasMany<ModelConfigs, Predictions>
}

export function initModelConfigs(sequelize: Sequelize) {
    ModelConfigs.init({
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        columns: DataTypes.ARRAY(DataTypes.STRING),
        min_coeff: DataTypes.FLOAT,
        max_coeff: DataTypes.FLOAT,
        bookmaker_margin: DataTypes.FLOAT,
        difference: DataTypes.FLOAT,
        config: DataTypes.JSONB,
        last_year_roi: DataTypes.FLOAT,
        last_year_profit: DataTypes.FLOAT,
        last_year_bets_number: DataTypes.FLOAT,
        in_use: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        }
    }, {sequelize, tableName: "model_configs"});

    Sports.ModelConfigs = Sports.hasMany(ModelConfigs, {foreignKey: {name: "sports_id", allowNull: false}, sourceKey: "id"});
    ModelConfigs.Sports = ModelConfigs.belongsTo(Sports, {foreignKey: {name: "sports_id", allowNull: false}, targetKey: "id"});

    Leagues.ModelConfigs = Leagues.hasMany(ModelConfigs, {foreignKey: {name: "leagues_id", allowNull: false}, sourceKey: "id"});
    ModelConfigs.Leagues = ModelConfigs.belongsTo(Leagues, {foreignKey: {name: "leagues_id", allowNull: false}, targetKey: "id"});

    Models.ModelConfigs = Models.hasMany(ModelConfigs, {foreignKey: {name: "models_id", allowNull: false}, sourceKey: "id"});
    ModelConfigs.Models = ModelConfigs.belongsTo(Models, {foreignKey: {name: "models_id", allowNull: false}, targetKey: "id"});

    Seasons.ModelConfigsTrain = Seasons.hasMany(ModelConfigs, {foreignKey: {name: "seasons_train_id", allowNull: true}, sourceKey: "id", as: "ModelConfigsTrain"});
    ModelConfigs.SeasonsTrain = ModelConfigs.belongsTo(Seasons, {foreignKey: {name: "seasons_train_id", allowNull: true}, targetKey: "id", as: "SeasonsTrain"});
}