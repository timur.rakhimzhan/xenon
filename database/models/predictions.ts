import {CustomModel} from "./custom";
import {Sequelize, BelongsTo, DataTypes} from "sequelize";
import {Statuses} from "./statuses";
import {Matches} from "./matches";
import {ModelConfigs} from "./modelConfigs";
import {Odds} from "./odds";

export class Predictions extends CustomModel {
    static Status: BelongsTo<Predictions, Statuses>;
    static Matches: BelongsTo<Predictions, Matches>;
    static ModelConfigs: BelongsTo<Predictions, ModelConfigs>;
    static Odds: BelongsTo<Predictions, Odds>
}

export function initPredictions(sequelize: Sequelize){
    Predictions.init({
        espn_predicted: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        date: DataTypes.DATE
    }, {sequelize, tableName: "predictions"});

    Matches.Predictions = Matches.hasMany(Predictions, {foreignKey: {name: "matches_id", allowNull: false}, sourceKey: "id"});
    Predictions.Matches = Predictions.belongsTo(Matches, {foreignKey: {name: "matches_id", allowNull: false}, targetKey: "id"});

    Statuses.Predictions = Statuses.hasMany(Predictions, {foreignKey: {name: "status_id", allowNull: false}, sourceKey: "id"});
    Predictions.Status = Predictions.belongsTo(Statuses, {foreignKey: {name: "status_id", allowNull: false}, targetKey: "id"});

    ModelConfigs.Predictions = ModelConfigs.hasMany(Predictions, {foreignKey: {name: "model_configs_id", allowNull: false}, sourceKey: "id"});
    Predictions.ModelConfigs = Predictions.belongsTo(ModelConfigs, {foreignKey: {name: "model_configs_id", allowNull: false}, targetKey: "id"});

    Odds.Predictions = Odds.hasMany(Predictions, {foreignKey: {name: "odds_id", allowNull: false}, sourceKey: "id"});
    Predictions.Odds = Predictions.belongsTo(Odds, {foreignKey: {name: "odds_id", allowNull: false}, targetKey: "id"});
}