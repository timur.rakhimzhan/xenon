"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const custom_1 = require("./custom");
class Sports extends custom_1.CustomModel {
}
exports.Sports = Sports;
function initSports(sequelize) {
    Sports.init({
        name: {
            type: sequelize_1.DataTypes.STRING,
            unique: true,
            allowNull: false
        },
        espn_name: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: false
        }
    }, { sequelize, tableName: 'sports' });
}
exports.initSports = initSports;
//# sourceMappingURL=sports.js.map