import {CustomModel} from "./custom";
import {DataTypes, Sequelize, HasMany} from "sequelize";
import {Statistics} from "./statistics";
import {Matches} from "./matches";
import {Predictions} from "./predictions";

export class Statuses extends CustomModel {
    static Statistics: HasMany<Statuses, Statistics>;
    static Matches: HasMany<Statuses, Matches>;
    static Predictions: HasMany<Statuses, Predictions>;
}

export function initStatus(sequelize: Sequelize) {
    Statuses.init({
        name: DataTypes.STRING,
        other_name: DataTypes.STRING,
        prediction: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        }
    }, {sequelize, tableName: "statuses"})
}