"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const custom_1 = require("./custom");
const sequelize_1 = require("sequelize");
class Statuses extends custom_1.CustomModel {
}
exports.Statuses = Statuses;
function initStatus(sequelize) {
    Statuses.init({
        name: sequelize_1.DataTypes.STRING,
        other_name: sequelize_1.DataTypes.STRING,
        prediction: {
            type: sequelize_1.DataTypes.BOOLEAN,
            defaultValue: false
        }
    }, { sequelize, tableName: "statuses" });
}
exports.initStatus = initStatus;
//# sourceMappingURL=statuses.js.map