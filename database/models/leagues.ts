import {BelongsTo, DataTypes, HasMany, Model, Sequelize} from "sequelize";
import {Sports} from "./sports";
import {CustomModel} from "./custom";
import {Teams} from "./teams";
import {Seasons} from "./seasons";
import {Statistics} from "./statistics";
import {Matches} from "./matches";
import {ModelConfigs} from "./modelConfigs";


export class Leagues extends CustomModel {
    static Sports: BelongsTo<Leagues, Sports>;
    static Teams: HasMany<Leagues, Teams>;
    static Seasons: HasMany<Leagues, Seasons>;
    static Statistics: HasMany<Leagues, Statistics>;
    static Matches: HasMany<Leagues, Matches>;
    static ModelConfigs: HasMany<Leagues, ModelConfigs>;
}

export function initLeagues(sequelize: Sequelize) {
    Leagues.init({
        name: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        },
        espn_name: {
            type: DataTypes.STRING,
            allowNull: false,
        }
    }, {sequelize, tableName: 'leagues'});

    Sports.Leagues = Sports.hasMany(Leagues, {foreignKey: {name: "sports_id", allowNull: false}, sourceKey: "id"});
    Leagues.Sports = Leagues.belongsTo(Sports, {foreignKey: {name: "sports_id", allowNull: false}, targetKey: "id"});
}