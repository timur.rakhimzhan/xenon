"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const custom_1 = require("./custom");
const sequelize_1 = require("sequelize");
class Models extends custom_1.CustomModel {
}
exports.Models = Models;
function initModels(sequelize) {
    Models.init({
        name: {
            type: sequelize_1.DataTypes.STRING,
            unique: true,
            allowNull: false
        }
    }, { sequelize, tableName: "models" });
}
exports.initModels = initModels;
//# sourceMappingURL=models.js.map