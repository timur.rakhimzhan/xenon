import {BelongsTo, DataTypes, HasMany, Model, Sequelize} from "sequelize";
import {Sports} from "./sports";
import {Leagues} from "./leagues";
import {Statistics} from "./statistics";
import {Matches} from "./matches";
import {ModelConfigs} from "./modelConfigs";
import {CustomModel} from "./custom";

export class Seasons extends CustomModel {
    static Sports: BelongsTo<Seasons, Sports>;
    static Leagues: BelongsTo<Seasons, Leagues>;
    static PreviousSeason: BelongsTo<Seasons, Seasons>;
    static Statistics: HasMany<Seasons, Statistics>;
    static Matches: HasMany<Seasons, Matches>;
    static ModelConfigsTrain: HasMany<Seasons, ModelConfigs>;
}
export function initSeasons(sequelize: Sequelize) {
    Seasons.init({
        name: DataTypes.STRING,
        start_date: {
            type: DataTypes.STRING(10),
        },
        end_date: {
            type: DataTypes.STRING(10),
        },
        current: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        }
    }, {tableName: 'seasons', sequelize});

    Sports.Seasons = Sports.hasMany(Seasons, {foreignKey: {name: "sports_id", allowNull: false}, sourceKey: "id"});
    Seasons.Sports = Seasons.belongsTo(Sports, {foreignKey: {name: "sports_id", allowNull: false}, targetKey: "id"});

    Leagues.Seasons = Leagues.hasMany(Seasons, {foreignKey: {name: "leagues_id", allowNull: false}, sourceKey: "id"});
    Seasons.Leagues = Seasons.belongsTo(Leagues, {foreignKey: {name: "leagues_id", allowNull: false}, targetKey: "id"});

    Seasons.PreviousSeason = Seasons.hasMany(Seasons, {foreignKey: {name: "previous_seasons_id", allowNull: true}, sourceKey: "id"});
}