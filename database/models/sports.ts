import {DataTypes, HasMany, Sequelize} from "sequelize";
import {Leagues} from "./leagues";
import {CustomModel} from "./custom";
import {Teams} from "./teams";
import {Statistics} from "./statistics";
import {Matches} from "./matches";
import {ModelConfigs} from "./modelConfigs";

export class Sports extends CustomModel{
    static Leagues: HasMany<Sports, Leagues>;
    static Teams: HasMany<Sports, Teams>;
    static Seasons: HasMany<Sports, Teams>;
    static Statistics: HasMany<Sports, Statistics>;
    static Matches: HasMany<Sports, Matches>;
    static ModelConfigs: HasMany<Sports, ModelConfigs>;
}

export function initSports(sequelize: Sequelize) {
    Sports.init({
        name: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        },
        espn_name: {
            type: DataTypes.STRING,
            allowNull: false
        }
    }, {sequelize, tableName: 'sports'});
}