import {Model, DataTypes, Sequelize, BelongsTo, Transaction} from "sequelize";
import {StatisticsDBI} from "../../typings/tablemaker/matchesDB";
import {Sports} from "./sports";
import {Leagues} from "./leagues";
import {Teams} from "./teams";
import {Seasons} from "./seasons";
import {Statuses} from "./statuses";
import store from "../../state/store";
import {State} from "../../typings/states";
import {createTableAction, establishMatchesAssociationAction} from "../../state/actions/tablemaker";

export class Statistics extends Model {
    static Sports: BelongsTo<Statistics, Sports>;
    static Leagues: BelongsTo<Statistics, Leagues>;
    static Seasons: BelongsTo<Statistics, Seasons>;
    static Status: BelongsTo<Statistics, Statuses>;
    static TeamsHome: BelongsTo<Statistics, Teams>;
    static TeamsAway: BelongsTo<Statistics, Teams>;
}

export async function initStatistics(matchDBQuery: StatisticsDBI, tableName: string, sequelize: Sequelize) {
    const {home_statistics, away_statistics} = matchDBQuery;
    const initOptions: Model['_attributes'] = {};
    console.log("Match table is created, statistics columns: ", Object.keys(home_statistics), Object.keys(away_statistics));
    Object.keys(home_statistics).map((key) => {
        initOptions[key] = DataTypes.FLOAT
    });
    Object.keys(away_statistics).map((key) => {
        initOptions[key] = DataTypes.FLOAT
    });
    Statistics.init({
        espn_id: DataTypes.INTEGER,
        home_score: DataTypes.INTEGER,
        home_coeff: DataTypes.FLOAT,
        home_prediction: DataTypes.FLOAT,
        home_pickcenter: DataTypes.JSONB,
        away_score: DataTypes.INTEGER,
        away_coeff: DataTypes.FLOAT,
        away_prediction: DataTypes.FLOAT,
        away_pickcenter: DataTypes.JSONB,
        neutral_site: DataTypes.BOOLEAN,
        periods: DataTypes.INTEGER,
        clock: DataTypes.FLOAT,
        start_date: DataTypes.DATE,
        ...initOptions,
    }, {sequelize, tableName});

    const {tablemaker}: State = store.getState();

    if(!tablemaker.tableAssociationsEstablished) {
        Sports.Statistics = Sports.hasMany(Statistics, {foreignKey: {name: "sports_id", allowNull: false}, sourceKey: "id"});
        Statistics.Sports = Statistics.belongsTo(Sports, {foreignKey: {name: "sports_id", allowNull: false}, targetKey: "id"});

        Leagues.Statistics = Leagues.hasMany(Statistics, {foreignKey: {name: "leagues_id", allowNull: false}, sourceKey: "id"});
        Statistics.Leagues = Statistics.belongsTo(Leagues, {foreignKey: {name: "leagues_id", allowNull: false}, targetKey: "id"});

        Seasons.Statistics = Seasons.hasMany(Statistics, {foreignKey: {name: "seasons_id", allowNull: false}, sourceKey: "id"});
        Statistics.Seasons = Statistics.belongsTo(Seasons, {foreignKey: {name: "seasons_id", allowNull: false}, targetKey: "id"});

        Statuses.Statistics = Statuses.hasMany(Statistics, {foreignKey: {name: "status_id", allowNull: false}, sourceKey: "id"});
        Statistics.Status = Statistics.belongsTo(Statuses, {foreignKey: {name: "status_id", allowNull: false}, targetKey: "id"});

        Teams.StatisticsHome = Teams.hasMany(Statistics, {foreignKey: {name: "home_id", allowNull: false}, sourceKey: "id", as: "StatisticsHome"});
        Statistics.TeamsHome = Statistics.belongsTo(Teams, {foreignKey: {name: "home_id", allowNull: false}, targetKey: "id", as: "TeamsHome"});

        Teams.StatisticsAway = Teams.hasMany(Statistics, {foreignKey: {name: "away_id", allowNull: false}, sourceKey: "id", as: "StatisticsAway"});
        Statistics.TeamsAway = Statistics.belongsTo(Teams, {foreignKey: {name: "away_id", allowNull: false}, targetKey: "id", as: "TeamsAway"});

        store.dispatch(establishMatchesAssociationAction());
    }
    store.dispatch(createTableAction());
    await Statistics.sync();

}