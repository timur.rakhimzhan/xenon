"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const sports_1 = require("./sports");
const leagues_1 = require("./leagues");
const teams_1 = require("./teams");
const seasons_1 = require("./seasons");
const statuses_1 = require("./statuses");
const store_1 = __importDefault(require("../../state/store"));
const tablemaker_1 = require("../../state/actions/tablemaker");
class Statistics extends sequelize_1.Model {
}
exports.Statistics = Statistics;
function initStatistics(matchDBQuery, tableName, sequelize) {
    return __awaiter(this, void 0, void 0, function* () {
        const { home_statistics, away_statistics } = matchDBQuery;
        const initOptions = {};
        console.log("Match table is created, statistics columns: ", Object.keys(home_statistics), Object.keys(away_statistics));
        Object.keys(home_statistics).map((key) => {
            initOptions[key] = sequelize_1.DataTypes.FLOAT;
        });
        Object.keys(away_statistics).map((key) => {
            initOptions[key] = sequelize_1.DataTypes.FLOAT;
        });
        Statistics.init(Object.assign({ espn_id: sequelize_1.DataTypes.INTEGER, home_score: sequelize_1.DataTypes.INTEGER, home_coeff: sequelize_1.DataTypes.FLOAT, home_prediction: sequelize_1.DataTypes.FLOAT, home_pickcenter: sequelize_1.DataTypes.JSONB, away_score: sequelize_1.DataTypes.INTEGER, away_coeff: sequelize_1.DataTypes.FLOAT, away_prediction: sequelize_1.DataTypes.FLOAT, away_pickcenter: sequelize_1.DataTypes.JSONB, neutral_site: sequelize_1.DataTypes.BOOLEAN, periods: sequelize_1.DataTypes.INTEGER, clock: sequelize_1.DataTypes.FLOAT, start_date: sequelize_1.DataTypes.DATE }, initOptions), { sequelize, tableName });
        const { tablemaker } = store_1.default.getState();
        if (!tablemaker.tableAssociationsEstablished) {
            sports_1.Sports.Statistics = sports_1.Sports.hasMany(Statistics, { foreignKey: { name: "sports_id", allowNull: false }, sourceKey: "id" });
            Statistics.Sports = Statistics.belongsTo(sports_1.Sports, { foreignKey: { name: "sports_id", allowNull: false }, targetKey: "id" });
            leagues_1.Leagues.Statistics = leagues_1.Leagues.hasMany(Statistics, { foreignKey: { name: "leagues_id", allowNull: false }, sourceKey: "id" });
            Statistics.Leagues = Statistics.belongsTo(leagues_1.Leagues, { foreignKey: { name: "leagues_id", allowNull: false }, targetKey: "id" });
            seasons_1.Seasons.Statistics = seasons_1.Seasons.hasMany(Statistics, { foreignKey: { name: "seasons_id", allowNull: false }, sourceKey: "id" });
            Statistics.Seasons = Statistics.belongsTo(seasons_1.Seasons, { foreignKey: { name: "seasons_id", allowNull: false }, targetKey: "id" });
            statuses_1.Statuses.Statistics = statuses_1.Statuses.hasMany(Statistics, { foreignKey: { name: "status_id", allowNull: false }, sourceKey: "id" });
            Statistics.Status = Statistics.belongsTo(statuses_1.Statuses, { foreignKey: { name: "status_id", allowNull: false }, targetKey: "id" });
            teams_1.Teams.StatisticsHome = teams_1.Teams.hasMany(Statistics, { foreignKey: { name: "home_id", allowNull: false }, sourceKey: "id", as: "StatisticsHome" });
            Statistics.TeamsHome = Statistics.belongsTo(teams_1.Teams, { foreignKey: { name: "home_id", allowNull: false }, targetKey: "id", as: "TeamsHome" });
            teams_1.Teams.StatisticsAway = teams_1.Teams.hasMany(Statistics, { foreignKey: { name: "away_id", allowNull: false }, sourceKey: "id", as: "StatisticsAway" });
            Statistics.TeamsAway = Statistics.belongsTo(teams_1.Teams, { foreignKey: { name: "away_id", allowNull: false }, targetKey: "id", as: "TeamsAway" });
            store_1.default.dispatch(tablemaker_1.establishMatchesAssociationAction());
        }
        store_1.default.dispatch(tablemaker_1.createTableAction());
        yield Statistics.sync();
    });
}
exports.initStatistics = initStatistics;
//# sourceMappingURL=statistics.js.map