"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const custom_1 = require("./custom");
const sequelize_1 = require("sequelize");
const statuses_1 = require("./statuses");
const matches_1 = require("./matches");
const modelConfigs_1 = require("./modelConfigs");
const odds_1 = require("./odds");
class Predictions extends custom_1.CustomModel {
}
exports.Predictions = Predictions;
function initPredictions(sequelize) {
    Predictions.init({
        espn_predicted: {
            type: sequelize_1.DataTypes.BOOLEAN,
            defaultValue: false
        },
        date: sequelize_1.DataTypes.DATE
    }, { sequelize, tableName: "predictions" });
    matches_1.Matches.Predictions = matches_1.Matches.hasMany(Predictions, { foreignKey: { name: "matches_id", allowNull: false }, sourceKey: "id" });
    Predictions.Matches = Predictions.belongsTo(matches_1.Matches, { foreignKey: { name: "matches_id", allowNull: false }, targetKey: "id" });
    statuses_1.Statuses.Predictions = statuses_1.Statuses.hasMany(Predictions, { foreignKey: { name: "status_id", allowNull: false }, sourceKey: "id" });
    Predictions.Status = Predictions.belongsTo(statuses_1.Statuses, { foreignKey: { name: "status_id", allowNull: false }, targetKey: "id" });
    modelConfigs_1.ModelConfigs.Predictions = modelConfigs_1.ModelConfigs.hasMany(Predictions, { foreignKey: { name: "model_configs_id", allowNull: false }, sourceKey: "id" });
    Predictions.ModelConfigs = Predictions.belongsTo(modelConfigs_1.ModelConfigs, { foreignKey: { name: "model_configs_id", allowNull: false }, targetKey: "id" });
    odds_1.Odds.Predictions = odds_1.Odds.hasMany(Predictions, { foreignKey: { name: "odds_id", allowNull: false }, sourceKey: "id" });
    Predictions.Odds = Predictions.belongsTo(odds_1.Odds, { foreignKey: { name: "odds_id", allowNull: false }, targetKey: "id" });
}
exports.initPredictions = initPredictions;
//# sourceMappingURL=predictions.js.map