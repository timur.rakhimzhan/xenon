import {connectDB} from "../connectDB";
import {questionPrompt} from "../../services/readline";
import {Seasons} from "../models/seasons";
import {Sequelize} from "sequelize";

async function main() {
    const sequelize: Sequelize = await connectDB();
    const sportName: string = await questionPrompt("Sport name:");
    const leagueName: string = await questionPrompt("League name:");
    const seasonName: string = await questionPrompt("Season Name:");
    const seasons = await Seasons.findAll({
        where: {
            name: seasonName,
        },
        include: [
            {
                association: Seasons.Sports,
                where: Sequelize.where(Sequelize.fn('lower', Sequelize.col('Sport.name')), Sequelize.fn('lower', sportName)),
            },
            {
                association: Seasons.Leagues,
                where: Sequelize.where(Sequelize.fn('lower', Sequelize.col('League.name')), Sequelize.fn('lower', leagueName)),

            }
        ]
    });
    for(let season of seasons) {
        await season.update({
            current: true
        });
    }
    await sequelize.close();
}

if(require.main === module) {
    main();
}