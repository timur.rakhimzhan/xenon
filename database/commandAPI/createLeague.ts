import {questionPrompt} from '../../services/readline';
import {connectDB} from "../connectDB";
import {Sequelize} from "sequelize";
import {Leagues} from "../models/leagues";
import {Sports} from "../models/sports";

async function main() {
    const sequelize: Sequelize = await connectDB();
    const sportName: string = await questionPrompt("Sport name:");
    const leagueName: string = await questionPrompt("League name:");
    const espnName: string = await questionPrompt("ESPN Name:");
    await Leagues.create({
        name: leagueName,
        espn_name: espnName,
        sports_id: await Sports.getId(Sequelize.where(Sequelize.fn('lower', Sequelize.col('name')), Sequelize.fn('lower', sportName)))});
    console.log(`League of ${sportName} with name: ${leagueName}, and espn name: ${espnName} has been added to database`);
    await sequelize.close();
}

if (require.main === module) {
    main();
}

