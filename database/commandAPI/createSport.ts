import {questionPrompt} from '../../services/readline';
import {Sports} from "../models/sports";
import {connectDB} from "../connectDB";
import {Sequelize} from "sequelize";

async function main() {
    const sequelize: Sequelize = await connectDB();
    const sportName: string = await questionPrompt("Sport name:");
    const espnName: string = await questionPrompt("ESPN Name:");
    await Sports.create({name: sportName, espn_name: espnName});
    console.log(`Sport with name: ${sportName}, and espn name: ${espnName} has been added to database`);
    await sequelize.close();
}

if (require.main === module) {
    main();
}

