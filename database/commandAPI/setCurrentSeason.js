"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const connectDB_1 = require("../connectDB");
const readline_1 = require("../../services/readline");
const seasons_1 = require("../models/seasons");
const sequelize_1 = require("sequelize");
function main() {
    return __awaiter(this, void 0, void 0, function* () {
        const sequelize = yield connectDB_1.connectDB();
        const sportName = yield readline_1.questionPrompt("Sport name:");
        const leagueName = yield readline_1.questionPrompt("League name:");
        const seasonName = yield readline_1.questionPrompt("Season Name:");
        const seasons = yield seasons_1.Seasons.findAll({
            where: {
                name: seasonName,
            },
            include: [
                {
                    association: seasons_1.Seasons.Sports,
                    where: sequelize_1.Sequelize.where(sequelize_1.Sequelize.fn('lower', sequelize_1.Sequelize.col('Sport.name')), sequelize_1.Sequelize.fn('lower', sportName)),
                },
                {
                    association: seasons_1.Seasons.Leagues,
                    where: sequelize_1.Sequelize.where(sequelize_1.Sequelize.fn('lower', sequelize_1.Sequelize.col('League.name')), sequelize_1.Sequelize.fn('lower', leagueName)),
                }
            ]
        });
        for (let season of seasons) {
            yield season.update({
                current: true
            });
        }
        yield sequelize.close();
    });
}
if (require.main === module) {
    main();
}
//# sourceMappingURL=setCurrentSeason.js.map