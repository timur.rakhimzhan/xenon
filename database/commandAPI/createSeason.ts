import {questionPrompt} from '../../services/readline';
import {Seasons} from "../models/seasons";
import {Sports} from "../models/sports";
import {Leagues} from "../models/leagues";
import {connectDB} from "../connectDB";
import {Sequelize} from "sequelize";

async function main() {
    const sequelize = await connectDB();
    const sportName: string = await questionPrompt("Sport name:");
    const leagueName: string = await questionPrompt("League name:");
    const seasonName: string = await questionPrompt("Season Name:");
    const startDate: string = await questionPrompt("Start date (YYYYMMDD):");
    const endDate: string = await questionPrompt("End date (YYYYMMDD):");
    const previousSeason: string = await questionPrompt("Previous season name:");
    await Seasons.create({
        name: seasonName,
        start_date: startDate,
        end_date: endDate,
        sports_id: await Sports.getId(Sequelize.where(Sequelize.fn('lower', Sequelize.col('name')), Sequelize.fn('lower', sportName))),
        leagues_id: await Leagues.getId(Sequelize.where(Sequelize.fn('lower', Sequelize.col('name')), Sequelize.fn('lower', leagueName))),
        previous_seasons_id: previousSeason ? await Seasons.getId(Sequelize.where(Sequelize.fn('lower', Sequelize.col('name')), Sequelize.fn('lower', previousSeason))) : null
    });
    console.log(`Season ${seasonName} of ${sportName} ${leagueName} with start date: ${startDate}, and end date: ${endDate} has been added to database`);
    await sequelize.close();
}

if (require.main === module) {
    main();
}

