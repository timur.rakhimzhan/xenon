import {DataTypes, Model, Sequelize} from "sequelize";
import * as dotenv from "dotenv";
import {initSports, Sports} from "./models/sports";
import {initLeagues, Leagues} from "./models/leagues";
import {initTeams, Teams} from "./models/teams";
import {initSeasons, Seasons} from "./models/seasons";
import {initStatus} from "./models/statuses";
import {initMatches} from "./models/matches";
import {initPredictions} from "./models/predictions";
import {initModels} from "./models/models";
import {initModelConfigs} from "./models/modelConfigs";
import {initOdds} from "./models/odds";
dotenv.config({path: `${__dirname}/../.env`});


export async function connectDB(): Promise<Sequelize> {
    const sequelize = new Sequelize({
        username: process.env.user,
        host: process.env.host,
        database: process.env.database,
        password: process.env.password,
        port: parseInt(process.env.port as string),
        dialect: "postgres",
        logging: false
    });
    try {
        await sequelize.authenticate();
        console.log('Connection has been established successfully.');
    } catch (error) {
        console.error('Unable to connect to the database:', error);
    }
    initSports(sequelize);
    initLeagues(sequelize);
    initTeams(sequelize);
    initSeasons(sequelize);
    initStatus(sequelize);
    initMatches(sequelize);
    initModels(sequelize);
    initModelConfigs(sequelize);
    initOdds(sequelize);
    initPredictions(sequelize);

    await sequelize.sync({alter: true});
    return sequelize;
}
