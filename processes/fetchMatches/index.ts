import {Sequelize} from "sequelize";
import {connectDB} from "../../database/connectDB";
import {Leagues} from "../../database/models/leagues";
import {Sports} from "../../database/models/sports";
import {fetchLeague} from "./fetchLeague";

export async function fetchMatches() {
    const sequelize: Sequelize = await connectDB();
    const leagues: Array<Leagues> = await Leagues.findAll({include: Leagues.Sports});
    for (let league of leagues) {
        const sportName = (league.get("Sport") as Sports).get("name") as string;
        const leagueName = league.get("name") as string;
        await fetchLeague(sportName, leagueName);
    }
    await sequelize.close();
}