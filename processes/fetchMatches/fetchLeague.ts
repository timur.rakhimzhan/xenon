import {Season} from "../../services/season";
import axios, {AxiosResponse} from "axios";
import moment from "moment";
import {espnScoreboardByDate} from "../../services/espnApi";
import {MatchDBI} from "../../typings/tablemaker/matchesDB";
import {getEventInfo} from "../../services/getEventInfo";
import {Matches} from "../../database/models/matches";
import {Statuses} from "../../database/models/statuses";
import {Teams} from "../../database/models/teams";

export async function fetchLeague(sportName: string, leagueName: string) {
    console.log(`Fetching of ${sportName} ${leagueName}`);
    const season: Season = new Season(sportName, leagueName);
    await season.init();
    let matches: AxiosResponse;
    const startDate = moment().subtract("1", "day");
    const endDate = moment().add("5", "day");

    try {
        matches = await axios.get(espnScoreboardByDate(season, startDate.format("YYYYMMDD"), endDate.format("YYYYMMDD")));
        for (let event of matches.data.events) {
            const {match, away, home, status}: MatchDBI = await getEventInfo(season, event);
            const alreadyInDB = await Matches.findAndCountAll({
                where: {
                    sports_id: season.sports_id,
                    leagues_id: season.leagues_id,
                    start_date: match.start_date,
                },
                include: [
                    {
                        association: Matches.TeamsHome,
                        where: {
                            abbreviation: home.abbreviation,
                        }
                    },
                    {
                        association: Matches.TeamsAway,
                        where: {
                            abbreviation: away.abbreviation
                        }
                    }
                ]
            });
            const [matchStatus] = await Statuses.findOrCreate({
                where: {
                    name: status
                }
            });
            if (alreadyInDB.count > 0) {
                const existingMatch: Matches = alreadyInDB.rows[0];
                await existingMatch.update({
                    status_id: matchStatus.get("id") as number,
                    ...match,
                });
                console.log(`Match updated: ${home.name}-${away.name} ${match.start_date}`)
            } else {
                const [teamHome] = await Teams.findOrCreate({
                    where: {
                        sports_id: season.sports_id,
                        leagues_id: season.leagues_id,
                        name: home.name,
                        abbreviation: home.abbreviation,
                        espn_id: home.espn_id
                    }
                });
                const [teamAway] = await Teams.findOrCreate({
                    where: {
                        sports_id: season.sports_id,
                        leagues_id: season.leagues_id,
                        name: away.name,
                        abbreviation: away.abbreviation,
                        espn_id: away.espn_id
                    }
                });

                await Matches.create({
                    sports_id: season.sports_id,
                    leagues_id: season.leagues_id,
                    seasons_id: season.id,
                    home_id: teamHome.get("id"),
                    away_id: teamAway.get("id"),
                    status_id: matchStatus.get("id"),
                    ...match,
                });
                console.log(`Match added ${home.name}-${away.name} ${match.start_date}`)
            }
        }

    } catch (error) {
        console.log("error while requesting espn api", error);
    }
}