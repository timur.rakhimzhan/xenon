"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const season_1 = require("../../services/season");
const axios_1 = __importDefault(require("axios"));
const moment_1 = __importDefault(require("moment"));
const espnApi_1 = require("../../services/espnApi");
const getEventInfo_1 = require("../../services/getEventInfo");
const matches_1 = require("../../database/models/matches");
const statuses_1 = require("../../database/models/statuses");
const teams_1 = require("../../database/models/teams");
function fetchLeague(sportName, leagueName) {
    return __awaiter(this, void 0, void 0, function* () {
        console.log(`Fetching of ${sportName} ${leagueName}`);
        const season = new season_1.Season(sportName, leagueName);
        yield season.init();
        let matches;
        const startDate = moment_1.default().subtract("1", "day");
        const endDate = moment_1.default().add("5", "day");
        try {
            matches = yield axios_1.default.get(espnApi_1.espnScoreboardByDate(season, startDate.format("YYYYMMDD"), endDate.format("YYYYMMDD")));
            for (let event of matches.data.events) {
                const { match, away, home, status } = yield getEventInfo_1.getEventInfo(season, event);
                const alreadyInDB = yield matches_1.Matches.findAndCountAll({
                    where: {
                        sports_id: season.sports_id,
                        leagues_id: season.leagues_id,
                        start_date: match.start_date,
                    },
                    include: [
                        {
                            association: matches_1.Matches.TeamsHome,
                            where: {
                                abbreviation: home.abbreviation,
                            }
                        },
                        {
                            association: matches_1.Matches.TeamsAway,
                            where: {
                                abbreviation: away.abbreviation
                            }
                        }
                    ]
                });
                const [matchStatus] = yield statuses_1.Statuses.findOrCreate({
                    where: {
                        name: status
                    }
                });
                if (alreadyInDB.count > 0) {
                    const existingMatch = alreadyInDB.rows[0];
                    yield existingMatch.update(Object.assign({ status_id: matchStatus.get("id") }, match));
                    console.log(`Match updated: ${home.name}-${away.name} ${match.start_date}`);
                }
                else {
                    const [teamHome] = yield teams_1.Teams.findOrCreate({
                        where: {
                            sports_id: season.sports_id,
                            leagues_id: season.leagues_id,
                            name: home.name,
                            abbreviation: home.abbreviation,
                            espn_id: home.espn_id
                        }
                    });
                    const [teamAway] = yield teams_1.Teams.findOrCreate({
                        where: {
                            sports_id: season.sports_id,
                            leagues_id: season.leagues_id,
                            name: away.name,
                            abbreviation: away.abbreviation,
                            espn_id: away.espn_id
                        }
                    });
                    yield matches_1.Matches.create(Object.assign({ sports_id: season.sports_id, leagues_id: season.leagues_id, seasons_id: season.id, home_id: teamHome.get("id"), away_id: teamAway.get("id"), status_id: matchStatus.get("id") }, match));
                    console.log(`Match added ${home.name}-${away.name} ${match.start_date}`);
                }
            }
        }
        catch (error) {
            console.log("error while requesting espn api", error);
        }
    });
}
exports.fetchLeague = fetchLeague;
//# sourceMappingURL=fetchLeague.js.map