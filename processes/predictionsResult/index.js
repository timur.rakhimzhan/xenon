"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const connectDB_1 = require("../../database/connectDB");
const predictions_1 = require("../../database/models/predictions");
const matches_1 = require("../../database/models/matches");
const statuses_1 = require("../../services/statuses");
const statuses_2 = require("../../database/models/statuses");
const spreadResult_1 = require("./spreadResult");
function predictionsResult() {
    return __awaiter(this, void 0, void 0, function* () {
        const sequelize = yield connectDB_1.connectDB();
        const pendingPredictions = yield predictions_1.Predictions.findAll({
            include: [
                {
                    association: predictions_1.Predictions.Matches,
                    include: [
                        {
                            association: matches_1.Matches.Status,
                            where: {
                                name: statuses_1.STATUS_FINAL
                            }
                        },
                        matches_1.Matches.TeamsHome,
                        matches_1.Matches.TeamsAway,
                        matches_1.Matches.Sports,
                        matches_1.Matches.Leagues
                    ],
                },
                {
                    association: predictions_1.Predictions.Status,
                    where: {
                        name: statuses_1.STATUS_PREDICTION_PENDING
                    }
                },
                {
                    association: predictions_1.Predictions.Odds
                }
            ]
        });
        for (const pendingPrediction of pendingPredictions) {
            const bet = pendingPrediction.get("Odd").get("name");
            const homeScore = pendingPrediction.get("Match").get("home_score");
            const awayScore = pendingPrediction.get("Match").get("away_score");
            let result;
            if (bet === "home") {
                if (homeScore > awayScore) {
                    result = statuses_1.STATUS_PREDICTION_WIN;
                }
                else {
                    result = statuses_1.STATUS_PREDICTION_LOSE;
                }
            }
            else {
                if (homeScore < awayScore) {
                    result = statuses_1.STATUS_PREDICTION_WIN;
                }
                else {
                    result = statuses_1.STATUS_PREDICTION_LOSE;
                }
            }
            const [status] = yield statuses_2.Statuses.findOrCreate({
                where: {
                    name: result,
                    prediction: true
                }
            });
            yield pendingPrediction.update({
                status_id: status.get("id")
            });
            const predictionResult = {
                bet,
                homeScore,
                awayScore,
                result,
                homeAbbrev: pendingPrediction.get("Match").get("TeamsHome").get("abbreviation"),
                home: pendingPrediction.get("Match").get("TeamsHome").get("name"),
                awayAbbrev: pendingPrediction.get("Match").get("TeamsAway").get("abbreviation"),
                away: pendingPrediction.get("Match").get("TeamsAway").get("name"),
                homeCoeff: pendingPrediction.get("Match").get("home_coeff"),
                awayCoeff: pendingPrediction.get("Match").get("away_coeff"),
                startDate: pendingPrediction.get("Match").get("start_date"),
                sportName: pendingPrediction.get("Match").get("Sport").get("name"),
                leagueName: pendingPrediction.get("Match").get("League").get("name"),
            };
            yield spreadResult_1.spreadResult(predictionResult);
        }
        yield sequelize.close();
    });
}
exports.predictionsResult = predictionsResult;
//# sourceMappingURL=index.js.map