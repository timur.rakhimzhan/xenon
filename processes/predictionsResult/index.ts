import {connectDB} from "../../database/connectDB";
import {Op, Sequelize} from "sequelize";
import {Predictions} from "../../database/models/predictions";
import {PredictionInfoI, PredictionResultI} from "../../typings/processes/prediction";
import {Odds} from "../../database/models/odds";
import {Matches} from "../../database/models/matches";
import {Teams} from "../../database/models/teams";
import {Sports} from "../../database/models/sports";
import {
    STATUS_FINAL,
    STATUS_PREDICTION_LOSE,
    STATUS_PREDICTION_PENDING,
    STATUS_PREDICTION_WIN
} from "../../services/statuses";
import {Statuses} from "../../database/models/statuses";
import {spreadResult} from "./spreadResult";

export async function predictionsResult() {
    const sequelize: Sequelize = await connectDB();
    const pendingPredictions = await Predictions.findAll({
        include: [
            {
                association: Predictions.Matches,
                include: [
                    {
                        association: Matches.Status,
                        where: {
                            name: STATUS_FINAL
                        }
                    },
                    Matches.TeamsHome,
                    Matches.TeamsAway,
                    Matches.Sports,
                    Matches.Leagues
                ],
            },
            {
                association: Predictions.Status,
                where: {
                    name: STATUS_PREDICTION_PENDING
                }
            },
            {
                association: Predictions.Odds
            }
        ]
    });
    for(const pendingPrediction of pendingPredictions) {
        const bet: string = (pendingPrediction.get("Odd") as Odds).get("name") as string;
        const homeScore: number = (pendingPrediction.get("Match") as Matches).get("home_score") as number;
        const awayScore: number = (pendingPrediction.get("Match") as Matches).get("away_score") as number;
        let result: string | undefined;
        if(bet === "home") {
            if(homeScore > awayScore) {
                result = STATUS_PREDICTION_WIN;
            } else {
                result = STATUS_PREDICTION_LOSE
            }
        } else {
            if(homeScore < awayScore) {
                result = STATUS_PREDICTION_WIN;
            } else {
                result = STATUS_PREDICTION_LOSE
            }
        }
        const [status] = await Statuses.findOrCreate({
            where: {
                name: result,
                prediction: true
            }
        });
        await pendingPrediction.update({
            status_id: status.get("id") as number
        });
        const predictionResult: PredictionResultI = {
            bet,
            homeScore,
            awayScore,
            result,
            homeAbbrev: ((pendingPrediction.get("Match") as Matches).get("TeamsHome") as Teams).get("abbreviation") as string,
            home: ((pendingPrediction.get("Match") as Matches).get("TeamsHome") as Teams).get("name") as string,
            awayAbbrev: ((pendingPrediction.get("Match") as Matches).get("TeamsAway") as Teams).get("abbreviation") as string,
            away: ((pendingPrediction.get("Match") as Matches).get("TeamsAway") as Teams).get("name") as string,
            homeCoeff: (pendingPrediction.get("Match") as Matches).get("home_coeff") as number,
            awayCoeff: (pendingPrediction.get("Match") as Matches).get("away_coeff") as number,
            startDate: (pendingPrediction.get("Match") as Matches).get("start_date") as string,
            sportName: ((pendingPrediction.get("Match") as Matches).get("Sport") as Sports).get("name") as string,
            leagueName: ((pendingPrediction.get("Match") as Matches).get("League") as Sports).get("name") as string,
        };
        await spreadResult(predictionResult);
    }
    await sequelize.close();
}