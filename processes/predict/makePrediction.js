"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const moment_1 = __importDefault(require("moment"));
const season_1 = require("../../services/season");
const modelConfigs_1 = require("../../database/models/modelConfigs");
const axios_1 = __importDefault(require("axios"));
const predictionsApi_1 = require("../../services/predictionsApi");
const managePrediction_1 = require("./managePrediction");
function makePrediction(matchUnfinished) {
    var _a;
    return __awaiter(this, void 0, void 0, function* () {
        const start_date = matchUnfinished.get("start_date");
        if (Math.abs(moment_1.default.duration(moment_1.default().diff(moment_1.default(start_date))).asHours()) < 1) {
            return;
        }
        const sportsId = matchUnfinished.get("sports_id");
        const leaguesId = matchUnfinished.get("leagues_id");
        const sportName = matchUnfinished.get("Sport").get("name");
        const leagueName = matchUnfinished.get("League").get("name");
        const season = new season_1.Season(sportName, leagueName);
        yield season.init();
        const modelConfigs = yield modelConfigs_1.ModelConfigs.findAll({
            where: {
                sports_id: sportsId,
                leagues_id: leaguesId,
                in_use: true
            },
            include: [modelConfigs_1.ModelConfigs.Sports, modelConfigs_1.ModelConfigs.Leagues, modelConfigs_1.ModelConfigs.Models, modelConfigs_1.ModelConfigs.SeasonsTrain]
        });
        const homeCoeff = matchUnfinished.get("home_coeff");
        const awayCoeff = matchUnfinished.get("away_coeff");
        if (!homeCoeff || !awayCoeff || homeCoeff <= 1 || awayCoeff <= 1) {
            return;
        }
        for (const modelConfig of modelConfigs) {
            const modelName = modelConfig.get("Model").get("name");
            const modelConfigName = modelConfig.get("name");
            if (!modelName.includes("ESPN")) {
                const homeAbbrev = matchUnfinished.get("TeamsHome").get("abbreviation");
                const awayAbbrev = matchUnfinished.get("TeamsAway").get("abbreviation");
                const neutralSite = matchUnfinished.get("neutral_site");
                const seasonsTrain = (_a = modelConfig.get("SeasonsTrain")) === null || _a === void 0 ? void 0 : _a.get("name");
                if (!homeAbbrev || !awayAbbrev || !seasonsTrain) {
                    continue;
                }
                const predictionResponse = yield axios_1.default.get(predictionsApi_1.predictionsApi(season, modelConfigName, false), {
                    params: {
                        home_coeff: homeCoeff,
                        away_coeff: awayCoeff,
                        home_abbreviation: homeAbbrev,
                        away_abbreviation: awayAbbrev,
                        season_train: seasonsTrain,
                        season: season.seasons_name,
                        start_date,
                        neutral_site: neutralSite
                    }
                });
                yield managePrediction_1.managePrediction(predictionResponse.data, matchUnfinished, modelConfig, false);
                console.log(`xenon prediction for ${homeAbbrev}-${awayAbbrev}:`);
                console.log(predictionResponse.data);
            }
            else {
                const homePrediction = matchUnfinished.get("home_prediction");
                const awayPrediction = matchUnfinished.get("away_prediction");
                if (!homePrediction || !awayPrediction) {
                    continue;
                }
                const predictionResponse = yield axios_1.default.get(predictionsApi_1.predictionsApi(season, modelConfigName, true), {
                    params: {
                        home_coeff: homeCoeff,
                        away_coeff: awayCoeff,
                        home_prediction: homePrediction,
                        away_prediction: awayPrediction
                    }
                });
                yield managePrediction_1.managePrediction(predictionResponse.data, matchUnfinished, modelConfig, true);
                console.log(`espn prediction for ${homePrediction}-${awayPrediction}:`);
                console.log(predictionResponse.data);
            }
        }
    });
}
exports.makePrediction = makePrediction;
//# sourceMappingURL=makePrediction.js.map