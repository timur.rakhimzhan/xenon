import {connectDB} from "../../database/connectDB";
import {Matches} from "../../database/models/matches";
import {Op} from "sequelize";
import {STATUS_FINAL} from "../../services/statuses";
import {updateResults} from "../matchesResult/updateResults";
import {makePrediction} from "./makePrediction";

async function predict() {
    const sequelize = await connectDB();
    const matchesUnfinished: Array<Matches> = await Matches.findAll({
        include: [
            Matches.Sports,
            Matches.Leagues,
            Matches.TeamsHome,
            Matches.TeamsAway,
            {
                association: Matches.Status,
                where: {
                    name: {
                        [Op.not]: STATUS_FINAL
                    }
                }
            }
        ]
    });
    for(let matchUnfinished of matchesUnfinished) {
        try{
            await makePrediction(matchUnfinished);
        } catch (e) {
            console.log(e.message);
        }
    }
    await sequelize.close();
}