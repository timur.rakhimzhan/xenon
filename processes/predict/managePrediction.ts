import {PredictionI, PredictionInfoI} from "../../typings/processes/prediction";
import {Matches} from "../../database/models/matches";
import {Odds} from "../../database/models/odds";
import {Predictions} from "../../database/models/predictions";
import {Statuses} from "../../database/models/statuses";
import {ModelConfigs} from "../../database/models/modelConfigs";
import moment from "moment";
import {Teams} from "../../database/models/teams";
import {spreadPrediction} from "./spreadPrediction";
import {Sports} from "../../database/models/sports";

export async function managePrediction(prediction: PredictionI, match: Matches, modelConfigs: ModelConfigs, espn: boolean) {
    if(prediction.bet === "none") {
        return;
    }
    const [odd]: [Odds, boolean] = await Odds.findOrCreate({
        where: {
            name: prediction.bet
        }
    });
    const [status]: [Statuses, boolean] = await Statuses.findOrCreate({
        where: {
            name: "STATUS_PREDICTION_PENDING",
            prediction: true
        }
    });
    const [, predictionExists]: [Predictions, boolean] = await Predictions.findOrCreate({
        where: {
            matches_id: match.get("id") as number,
            odds_id: odd.get("id") as number,
            status_id: status.get("id") as number,
            model_configs_id: modelConfigs.get("id") as number,
            espn_predicted: espn,
            date: moment()
        }
    });
    const predictionInfo: PredictionInfoI = {
        ...prediction,
        sportName: (modelConfigs.get("Sport") as Sports).get("name") as string,
        leagueName: (modelConfigs.get("League") as Sports).get("name") as string,
        home: (match.get("TeamsHome") as Teams).get("name") as string,
        homeAbbrev: (match.get("TeamsHome") as Teams).get("abbreviation") as string,
        away: (match.get("TeamsAway") as Teams).get("name") as string,
        awayAbbrev: (match.get("TeamsAway") as Teams).get("abbreviation") as string,
        homeCoeff: match.get("home_coeff") as number,
        awayCoeff: match.get("away_coeff") as number,
        startDate: match.get("start_date") as string,
    };
    if(!predictionExists) {
        await spreadPrediction(predictionInfo);
    }
}