"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const odds_1 = require("../../database/models/odds");
const predictions_1 = require("../../database/models/predictions");
const statuses_1 = require("../../database/models/statuses");
const moment_1 = __importDefault(require("moment"));
const spreadPrediction_1 = require("./spreadPrediction");
function managePrediction(prediction, match, modelConfigs, espn) {
    return __awaiter(this, void 0, void 0, function* () {
        if (prediction.bet === "none") {
            return;
        }
        const [odd] = yield odds_1.Odds.findOrCreate({
            where: {
                name: prediction.bet
            }
        });
        const [status] = yield statuses_1.Statuses.findOrCreate({
            where: {
                name: "STATUS_PREDICTION_PENDING",
                prediction: true
            }
        });
        const [, predictionExists] = yield predictions_1.Predictions.findOrCreate({
            where: {
                matches_id: match.get("id"),
                odds_id: odd.get("id"),
                status_id: status.get("id"),
                model_configs_id: modelConfigs.get("id"),
                espn_predicted: espn,
                date: moment_1.default()
            }
        });
        const predictionInfo = Object.assign(Object.assign({}, prediction), { sportName: modelConfigs.get("Sport").get("name"), leagueName: modelConfigs.get("League").get("name"), home: match.get("TeamsHome").get("name"), homeAbbrev: match.get("TeamsHome").get("abbreviation"), away: match.get("TeamsAway").get("name"), awayAbbrev: match.get("TeamsAway").get("abbreviation"), homeCoeff: match.get("home_coeff"), awayCoeff: match.get("away_coeff"), startDate: match.get("start_date") });
        if (!predictionExists) {
            yield spreadPrediction_1.spreadPrediction(predictionInfo);
        }
    });
}
exports.managePrediction = managePrediction;
//# sourceMappingURL=managePrediction.js.map