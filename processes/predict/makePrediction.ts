import {Matches} from "../../database/models/matches";
import moment from "moment";
import {Sports} from "../../database/models/sports";
import {Season} from "../../services/season";
import {ModelConfigs} from "../../database/models/modelConfigs";
import {Models} from "../../database/models/models";
import {Teams} from "../../database/models/teams";
import {Seasons} from "../../database/models/seasons";
import axios, {AxiosResponse} from "axios";
import {predictionsApi} from "../../services/predictionsApi";
import {PredictionI} from "../../typings/processes/prediction";
import {managePrediction} from "./managePrediction";

export async function makePrediction(matchUnfinished: Matches) {
    const start_date: string = matchUnfinished.get("start_date") as string;
    if (Math.abs(moment.duration(moment().diff(moment(start_date))).asHours()) < 1) {
        return;
    }

    const sportsId: number = matchUnfinished.get("sports_id") as number;
    const leaguesId: number = matchUnfinished.get("leagues_id") as number;
    const sportName: string = (matchUnfinished.get("Sport") as Sports).get("name") as string;
    const leagueName: string = (matchUnfinished.get("League") as Sports).get("name") as string;
    const season = new Season(sportName, leagueName);
    await season.init();
    const modelConfigs = await ModelConfigs.findAll({
        where: {
            sports_id: sportsId,
            leagues_id: leaguesId,
            in_use: true
        },
        include: [ModelConfigs.Sports, ModelConfigs.Leagues, ModelConfigs.Models, ModelConfigs.SeasonsTrain]
    });
    const homeCoeff: number = matchUnfinished.get("home_coeff") as number;
    const awayCoeff: number = matchUnfinished.get("away_coeff") as number;
    if (!homeCoeff || !awayCoeff || homeCoeff <= 1 || awayCoeff <= 1) {
        return;
    }
    for (const modelConfig of modelConfigs) {
        const modelName: string = (modelConfig.get("Model") as Models).get("name") as string;
        const modelConfigName: string = modelConfig.get("name") as string;
        if (!modelName.includes("ESPN")) {
            const homeAbbrev: string = (matchUnfinished.get("TeamsHome") as Teams).get("abbreviation") as string;
            const awayAbbrev: string = (matchUnfinished.get("TeamsAway") as Teams).get("abbreviation") as string;
            const neutralSite: boolean = matchUnfinished.get("neutral_site") as boolean;
            const seasonsTrain: string | null = (modelConfig.get("SeasonsTrain") as Seasons)?.get("name") as string | null;
            if (!homeAbbrev || !awayAbbrev || !seasonsTrain) {
                continue;
            }
            const predictionResponse: AxiosResponse<PredictionI> = await axios.get(predictionsApi(season, modelConfigName, false), {
                params: {
                    home_coeff: homeCoeff,
                    away_coeff: awayCoeff,
                    home_abbreviation: homeAbbrev,
                    away_abbreviation: awayAbbrev,
                    season_train: seasonsTrain,
                    season: season.seasons_name,
                    start_date,
                    neutral_site: neutralSite
                }
            });
            await managePrediction(predictionResponse.data, matchUnfinished, modelConfig, false);
            console.log(`xenon prediction for ${homeAbbrev}-${awayAbbrev}:`);
            console.log(predictionResponse.data);
        } else {
            const homePrediction: number = matchUnfinished.get("home_prediction") as number;
            const awayPrediction: number = matchUnfinished.get("away_prediction") as number;
            if (!homePrediction || !awayPrediction) {
                continue;
            }
            const predictionResponse: AxiosResponse<PredictionI> = await axios.get(predictionsApi(season, modelConfigName, true), {
                params: {
                    home_coeff: homeCoeff,
                    away_coeff: awayCoeff,
                    home_prediction: homePrediction,
                    away_prediction: awayPrediction
                }
            });
            await managePrediction(predictionResponse.data, matchUnfinished, modelConfig, true);
            console.log(`espn prediction for ${homePrediction}-${awayPrediction}:`);
            console.log(predictionResponse.data)
        }
    }
}