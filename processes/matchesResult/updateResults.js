"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const season_1 = require("../../services/season");
const axios_1 = __importDefault(require("axios"));
const espnApi_1 = require("../../services/espnApi");
const getEventInfo_1 = require("../../services/getEventInfo");
const statuses_1 = require("../../database/models/statuses");
function updateResults(matchUnfinished) {
    var _a;
    return __awaiter(this, void 0, void 0, function* () {
        const sportName = matchUnfinished.get("Sport").get("name");
        const leagueName = matchUnfinished.get("League").get("name");
        const season = new season_1.Season(sportName, leagueName);
        yield season.init();
        const espnId = matchUnfinished.get("espn_id");
        const eventScoreboard = (_a = (yield axios_1.default.get(espnApi_1.espnScoreboardById(season, espnId)))) === null || _a === void 0 ? void 0 : _a.data;
        const { status, match, home, away } = yield getEventInfo_1.getEventInfo(season, eventScoreboard);
        const [matchStatus] = yield statuses_1.Statuses.findOrCreate({
            where: {
                name: status,
            }
        });
        yield matchUnfinished.update(Object.assign(Object.assign({}, match), { status_id: matchStatus.get("id") }));
        if (status.includes("FINAL")) {
            console.log(`${home.name}-${away.name} ${match.start_date} updated`);
        }
    });
}
exports.updateResults = updateResults;
//# sourceMappingURL=updateResults.js.map