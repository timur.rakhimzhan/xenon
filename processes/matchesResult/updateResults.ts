import {Matches} from "../../database/models/matches";
import {Sports} from "../../database/models/sports";
import {Season} from "../../services/season";
import {EspnScoreboardI} from "../../typings/tablemaker/espn";
import axios from "axios";
import {espnScoreboardById} from "../../services/espnApi";
import {getEventInfo} from "../../services/getEventInfo";
import {Statuses} from "../../database/models/statuses";

export async function updateResults(matchUnfinished: Matches) {
    const sportName: string = (matchUnfinished.get("Sport") as Sports).get("name") as string;
    const leagueName: string = (matchUnfinished.get("League") as Sports).get("name") as string;
    const season = new Season(sportName, leagueName);
    await season.init();
    const espnId: string = matchUnfinished.get("espn_id") as string;
    const eventScoreboard: EspnScoreboardI = (await axios.get(espnScoreboardById(season, espnId)))?.data;
    const {status, match, home, away} = await getEventInfo(season, eventScoreboard);
    const [matchStatus] = await Statuses.findOrCreate({
        where: {
            name: status,
        }
    });
    await matchUnfinished.update({
        ...match,
        status_id: matchStatus.get("id")
    });
    if (status.includes("FINAL")) {
        console.log(`${home.name}-${away.name} ${match.start_date} updated`)
    }
}