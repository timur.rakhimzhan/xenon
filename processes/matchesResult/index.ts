import {Matches} from "../../database/models/matches";
import {connectDB} from "../../database/connectDB";
import {Op} from "sequelize";
import {STATUS_FINAL} from "../../services/statuses";
import {updateResults} from "./updateResults";

async function matchesResult() {
    const sequelize = await connectDB();
    const matchesUnfinished: Array<Matches> = await Matches.findAll({
        include: [
            Matches.Sports,
            Matches.Leagues,
            {
                association: Matches.Status,
                where: {
                    name: {
                        [Op.not]: STATUS_FINAL
                    }
                }
            }
        ]
    });
    for(let matchUnfinished of matchesUnfinished) {
        try{
            await updateResults(matchUnfinished);
        } catch (e) {
            console.log(e.message);
        }
    }
    await sequelize.close();
}

