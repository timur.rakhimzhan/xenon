"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const matches_1 = require("../../database/models/matches");
const connectDB_1 = require("../../database/connectDB");
const sequelize_1 = require("sequelize");
const statuses_1 = require("../../services/statuses");
const updateResults_1 = require("./updateResults");
function matchesResult() {
    return __awaiter(this, void 0, void 0, function* () {
        const sequelize = yield connectDB_1.connectDB();
        const matchesUnfinished = yield matches_1.Matches.findAll({
            include: [
                matches_1.Matches.Sports,
                matches_1.Matches.Leagues,
                {
                    association: matches_1.Matches.Status,
                    where: {
                        name: {
                            [sequelize_1.Op.not]: statuses_1.STATUS_FINAL
                        }
                    }
                }
            ]
        });
        for (let matchUnfinished of matchesUnfinished) {
            try {
                yield updateResults_1.updateResults(matchUnfinished);
            }
            catch (e) {
                console.log(e.message);
            }
        }
        yield sequelize.close();
    });
}
//# sourceMappingURL=index.js.map