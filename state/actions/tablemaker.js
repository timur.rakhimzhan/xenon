"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const actionTypes_1 = require("./actionTypes");
function setStatsHomeAction(columns) {
    return {
        type: actionTypes_1.SET_HOME_STATISTICS_COLUMNS,
        payload: { statisticsColumnsHome: columns }
    };
}
exports.setStatsHomeAction = setStatsHomeAction;
function setStatsAwayAction(columns) {
    return {
        type: actionTypes_1.SET_AWAY_STATISTICS_COLUMNS,
        payload: { statisticsColumnsAway: columns }
    };
}
exports.setStatsAwayAction = setStatsAwayAction;
function establishMatchesAssociationAction() {
    return {
        type: actionTypes_1.ESTABLISH_MATCH_ASSOCIATIONS,
        payload: {}
    };
}
exports.establishMatchesAssociationAction = establishMatchesAssociationAction;
function createTableAction() {
    return {
        type: actionTypes_1.CREATE_TABLE,
        payload: {}
    };
}
exports.createTableAction = createTableAction;
//# sourceMappingURL=tablemaker.js.map