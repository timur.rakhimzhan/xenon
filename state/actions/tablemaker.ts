import {
    CREATE_TABLE,
    ESTABLISH_MATCH_ASSOCIATIONS,
    SET_AWAY_STATISTICS_COLUMNS,
    SET_HOME_STATISTICS_COLUMNS
} from "./actionTypes";
import {TablemakerAction} from "../../typings/actions";
import {TablemakerState} from "../../typings/states";

export function setStatsHomeAction(columns: Set<string>): TablemakerAction {
    return {
        type: SET_HOME_STATISTICS_COLUMNS,
        payload: { statisticsColumnsHome: columns }
    }
}

export function setStatsAwayAction(columns: Set<string>): TablemakerAction {
    return {
        type: SET_AWAY_STATISTICS_COLUMNS,
        payload: { statisticsColumnsAway: columns }
    }
}

export function establishMatchesAssociationAction(): TablemakerAction {
    return  {
        type: ESTABLISH_MATCH_ASSOCIATIONS,
        payload: {}
    }
}

export function createTableAction(): TablemakerAction {
    return {
        type: CREATE_TABLE,
        payload: {}
    }
}