"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SET_HOME_STATISTICS_COLUMNS = 'SET_HOME_STATISTICS_COLUMNS';
exports.SET_AWAY_STATISTICS_COLUMNS = 'SET_AWAY_STATISTICS_COLUMNS';
exports.ESTABLISH_MATCH_ASSOCIATIONS = 'ESTABLISH_MATCH_ASSOCIATIONS';
exports.CREATE_TABLE = 'CREATE_TABLE';
exports.SET_DAEMON = 'SET_DAEMON';
//# sourceMappingURL=index.js.map