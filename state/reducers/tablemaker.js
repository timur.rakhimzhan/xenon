"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const actionTypes_1 = require("../actions/actionTypes");
const initialState = {
    tableAssociationsEstablished: false
};
function tablemakerStateReducer(state = initialState, action) {
    switch (action.type) {
        case actionTypes_1.SET_HOME_STATISTICS_COLUMNS:
            return Object.assign(Object.assign({}, state), { statisticsColumnsHome: action.payload.statisticsColumnsHome });
        case actionTypes_1.SET_AWAY_STATISTICS_COLUMNS:
            return Object.assign(Object.assign({}, state), { statisticsColumnsAway: action.payload.statisticsColumnsAway });
        case actionTypes_1.ESTABLISH_MATCH_ASSOCIATIONS:
            return Object.assign(Object.assign({}, state), { tableAssociationsEstablished: true });
        case actionTypes_1.CREATE_TABLE:
            return Object.assign(Object.assign({}, state), { tableCreated: true });
        default:
            return state;
    }
}
exports.tablemakerStateReducer = tablemakerStateReducer;
//# sourceMappingURL=tablemaker.js.map