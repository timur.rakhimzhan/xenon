"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const actionTypes_1 = require("../actions/actionTypes");
const initialState = {
    daemon: false
};
function fetcherStateReducer(state = initialState, action) {
    switch (action.type) {
        case actionTypes_1.SET_DAEMON:
            return Object.assign(Object.assign({}, state), { daemon: true });
        default:
            return state;
    }
}
exports.fetcherStateReducer = fetcherStateReducer;
//# sourceMappingURL=fetcher.js.map