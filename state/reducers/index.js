"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tablemaker_1 = require("./tablemaker");
const redux_1 = require("redux");
const fetcher_1 = require("./fetcher");
exports.default = redux_1.combineReducers({ tablemaker: tablemaker_1.tablemakerStateReducer, fetcher: fetcher_1.fetcherStateReducer });
//# sourceMappingURL=index.js.map