import {tablemakerStateReducer} from "./tablemaker";
import {combineReducers} from "redux";
import {fetcherStateReducer} from "./fetcher";


export default combineReducers({tablemaker: tablemakerStateReducer, fetcher: fetcherStateReducer});