import {
    SET_HOME_STATISTICS_COLUMNS,
    SET_AWAY_STATISTICS_COLUMNS,
    ESTABLISH_MATCH_ASSOCIATIONS, CREATE_TABLE
} from "../actions/actionTypes";
import {TablemakerAction} from "../../typings/actions";
import {TablemakerState} from "../../typings/states";

const initialState: TablemakerState = {
    tableAssociationsEstablished: false
};

export function tablemakerStateReducer(state: TablemakerState = initialState, action: TablemakerAction): TablemakerState {
    switch(action.type){
        case SET_HOME_STATISTICS_COLUMNS:
            return {...state, statisticsColumnsHome: action.payload.statisticsColumnsHome};
        case SET_AWAY_STATISTICS_COLUMNS:
            return {...state, statisticsColumnsAway: action.payload.statisticsColumnsAway};
        case ESTABLISH_MATCH_ASSOCIATIONS:
            return {...state, tableAssociationsEstablished: true};
        case CREATE_TABLE:
            return {...state, tableCreated: true};
        default:
            return state;
    }
}