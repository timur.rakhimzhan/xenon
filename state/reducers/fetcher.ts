import {FetcherState, TablemakerState} from "../../typings/states";
import {FetcherAction, TablemakerAction} from "../../typings/actions";
import { SET_DAEMON } from "../actions/actionTypes";

const initialState: FetcherState = {
    daemon: false
};


export function fetcherStateReducer(state: FetcherState = initialState, action: FetcherAction): FetcherState {
    switch(action.type){
        case SET_DAEMON:
            return {...state, daemon: true};
        default:
            return state;
    }
}