"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const moment_1 = __importDefault(require("moment"));
const helpers_1 = require("./helpers");
const databaseRequests_1 = require("./databaseRequests");
const connectDB_1 = require("../database/connectDB");
const season_1 = require("../services/season");
const store_1 = __importDefault(require("../state/store"));
const tablemaker_1 = require("../state/actions/tablemaker");
const espnApi_1 = require("../services/espnApi");
function main(sport, league, seasonName) {
    return __awaiter(this, void 0, void 0, function* () {
        const sequelize = yield connectDB_1.connectDB();
        const season = new season_1.Season(sport, league, seasonName);
        yield season.init();
        let startDate = moment_1.default(season.start_date, "YYYYMMDD");
        let endDate = moment_1.default(startDate).add(1, "day");
        const finishDate = season.current ? moment_1.default() : moment_1.default(season.end_date, "YYYYMMDD");
        let totalFromApi = 0;
        let totalFromDb = 0;
        const promisesDB = [];
        while (true) {
            const { tablemaker } = store_1.default.getState();
            const matches = yield helpers_1.repeatHOF(() => __awaiter(this, void 0, void 0, function* () {
                var _a, _b, _c, _d;
                try {
                    return yield axios_1.default.get(espnApi_1.espnScoreboardByDate(season, startDate.format("YYYYMMDD"), endDate.format("YYYYMMDD")), { timeout: 30000 });
                }
                catch (error) {
                    if (((_b = (_a = error) === null || _a === void 0 ? void 0 : _a.response) === null || _b === void 0 ? void 0 : _b.status) === 404 || ((_d = (_c = error) === null || _c === void 0 ? void 0 : _c.response) === null || _d === void 0 ? void 0 : _d.status) === 502) {
                        return;
                    }
                    throw error;
                }
            }));
            if (!matches) {
                startDate = moment_1.default(endDate);
                endDate = moment_1.default(startDate).add(1, "day");
                continue;
            }
            totalFromApi += matches.data.events.length;
            console.log("progress...");
            for (let eventScoreboard of matches.data.events) {
                if (!tablemaker.tableCreated) {
                    yield helpers_1.repeatHOF(databaseRequests_1.databaseRequests, season, eventScoreboard, sequelize);
                    store_1.default.dispatch(tablemaker_1.createTableAction());
                    continue;
                }
                promisesDB.push(helpers_1.repeatHOF(databaseRequests_1.databaseRequests, season, eventScoreboard, sequelize));
                if (promisesDB.length === 50) {
                    yield Promise.all(promisesDB);
                    totalFromDb += 50;
                    console.log(totalFromDb);
                    promisesDB.length = 0;
                }
            }
            if (finishDate.isBefore(endDate) || finishDate.isSame(endDate)) {
                break;
            }
            startDate = moment_1.default(endDate);
            endDate = moment_1.default(startDate).add(1, "day");
        }
        const added = yield Promise.all(promisesDB);
        console.log(`Finished. API matches: ${totalFromApi}, added: ${totalFromDb + added.length + 1}`);
        yield sequelize.close();
    });
}
main(process.argv[2], process.argv[3], process.argv[4]);
//# sourceMappingURL=index.js.map