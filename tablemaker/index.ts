import axios, {AxiosResponse} from 'axios';
import moment, {Moment} from 'moment';
import {repeatHOF} from "./helpers";
import {databaseRequests} from "./databaseRequests";
import {Sequelize, DataTypes} from "sequelize";
import {connectDB} from "../database/connectDB";
import {Season} from "../services/season";
import {State} from "../typings/states";
import store from "../state/store";
import {createTableAction} from "../state/actions/tablemaker";
import {espnScoreboardByDate} from "../services/espnApi";

async function main(sport: string, league: string, seasonName: string): Promise<void> {
    const sequelize: Sequelize = await connectDB();
    const season: Season = new Season(sport, league, seasonName);
    await season.init();
    let startDate: Moment = moment(season.start_date, "YYYYMMDD");
    let endDate: Moment = moment(startDate).add(1, "day");
    const finishDate: Moment = season.current ? moment() : moment(season.end_date, "YYYYMMDD");

    let totalFromApi = 0;
    let totalFromDb = 0;
    const promisesDB: Array<Promise<any>> = [];

    while(true) {
        const {tablemaker}: State = store.getState();
        const matches: AxiosResponse | undefined = await repeatHOF(async () => {
            try {
                return await axios.get(espnScoreboardByDate(season, startDate.format("YYYYMMDD"), endDate.format("YYYYMMDD")), {timeout: 30000});
            } catch(error) {
                if(error?.response?.status === 404 || error?.response?.status === 502) {
                    return
                }
                throw error;
            }
        });
        if(!matches) {
            startDate = moment(endDate);
            endDate = moment(startDate).add(1, "day");
            continue;
        }
        totalFromApi += matches.data.events.length;
        console.log("progress...");

        for(let eventScoreboard of matches.data.events) {
            if(!tablemaker.tableCreated) {
                await repeatHOF(databaseRequests, season, eventScoreboard, sequelize);
                store.dispatch(createTableAction());
                continue;
            }
            promisesDB.push(repeatHOF(databaseRequests, season, eventScoreboard, sequelize));
            if(promisesDB.length === 50) {
                await Promise.all(promisesDB);
                totalFromDb += 50;
                console.log(totalFromDb);
                promisesDB.length = 0;
            }
        }
        if(finishDate.isBefore(endDate) || finishDate.isSame(endDate)) {
            break;
        }
        startDate = moment(endDate);
        endDate = moment(startDate).add(1, "day");
    }
    const added = await Promise.all(promisesDB);
    console.log(`Finished. API matches: ${totalFromApi}, added: ${totalFromDb + added.length + 1}`);
    await sequelize.close();
}


main(process.argv[2], process.argv[3], process.argv[4]);
