"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const statistics_1 = require("../../database/models/statistics");
const teams_1 = require("../../database/models/teams");
const statuses_1 = require("../../database/models/statuses");
function createMatchRecord(season, statisticsDB) {
    return __awaiter(this, void 0, void 0, function* () {
        const { match, home, away, home_statistics, away_statistics, status } = statisticsDB;
        const alreadyInDB = yield statistics_1.Statistics.count({
            where: {
                sports_id: season.sports_id,
                leagues_id: season.leagues_id,
                start_date: match.start_date
            },
            include: [
                {
                    association: statistics_1.Statistics.TeamsHome,
                    where: {
                        abbreviation: home.abbreviation,
                    }
                },
                {
                    association: statistics_1.Statistics.TeamsAway,
                    where: {
                        abbreviation: away.abbreviation
                    }
                }
            ]
        });
        if (alreadyInDB > 0) {
            return "Match already in database";
        }
        // if(Object.keys(home_statistics).length === 0) {
        //     return "Empty statistics";
        // }
        const [teamHome] = yield teams_1.Teams.findOrCreate({
            where: {
                sports_id: season.sports_id,
                leagues_id: season.leagues_id,
                name: home.name,
                abbreviation: home.abbreviation,
                espn_id: home.espn_id
            }
        });
        const [teamAway] = yield teams_1.Teams.findOrCreate({
            where: {
                sports_id: season.sports_id,
                leagues_id: season.leagues_id,
                name: away.name,
                abbreviation: away.abbreviation,
                espn_id: away.espn_id
            }
        });
        const [matchStatus] = yield statuses_1.Statuses.findOrCreate({
            where: {
                name: status
            }
        });
        yield statistics_1.Statistics.create(Object.assign(Object.assign(Object.assign({ sports_id: season.sports_id, leagues_id: season.leagues_id, seasons_id: season.id, home_id: teamHome.get("id"), away_id: teamAway.get("id"), status_id: matchStatus.get("id") }, match), home_statistics), away_statistics));
    });
}
exports.createMatchRecord = createMatchRecord;
//# sourceMappingURL=createMatchRecord.js.map