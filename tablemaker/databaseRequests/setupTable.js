"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const store_1 = __importDefault(require("../../state/store"));
const tablemaker_1 = require("../../state/actions/tablemaker");
const statistics_1 = require("../../database/models/statistics");
function setupTable(season, statisticsDB, sequelize) {
    return __awaiter(this, void 0, void 0, function* () {
        const { tablemaker } = store_1.default.getState();
        const { home_statistics, away_statistics } = statisticsDB;
        if (!tablemaker.statisticsColumnsHome || !tablemaker.statisticsColumnsAway) {
            store_1.default.dispatch(tablemaker_1.setStatsHomeAction(new Set(Object.keys(home_statistics))));
            store_1.default.dispatch(tablemaker_1.setStatsAwayAction(new Set(Object.keys(away_statistics))));
            return yield statistics_1.initStatistics(statisticsDB, season.matchesTableName, sequelize);
        }
        // const statisticsColumnsHome = new Set(Object.keys(home_statistics));
        // const statisticsColumnsAway = new Set(Object.keys(away_statistics));
        // const differenceHome = setDifference(statisticsColumnsHome, tablemaker.statisticsColumnsHome);
        // const differenceAway = setDifference(statisticsColumnsAway, tablemaker.statisticsColumnsAway);
        //
        // if(differenceHome.size || differenceAway.size) {
        //     store.dispatch(setStatsHomeAction(new Set([...statisticsColumnsHome, ...tablemaker.statisticsColumnsHome])));
        //     store.dispatch(setStatsAwayAction(new Set([...statisticsColumnsAway, ...tablemaker.statisticsColumnsAway])));
        //     return await initStatistics(statisticsDB, season.matchesTableName, sequelize);
        // }
    });
}
exports.setupTable = setupTable;
//# sourceMappingURL=setupTable.js.map