import {StatisticsDBI} from "../../typings/tablemaker/matchesDB";
import {Sequelize} from "sequelize";
import {Statistics} from "../../database/models/statistics";
import {Teams} from "../../database/models/teams";
import {Statuses} from "../../database/models/statuses";
import {Season} from "../../services/season";
import store from "../../state/store";

export async function createMatchRecord(season: Season, statisticsDB: StatisticsDBI) {
    const {match, home, away, home_statistics, away_statistics, status} = statisticsDB;
    const alreadyInDB = await Statistics.count({
        where: {
            sports_id: season.sports_id,
            leagues_id: season.leagues_id,
            start_date: match.start_date
        },
        include: [
            {
                association: Statistics.TeamsHome,
                where: {
                    abbreviation: home.abbreviation,
                }
            },
            {
                association: Statistics.TeamsAway,
                where: {
                    abbreviation: away.abbreviation
                }
            }
        ]
    });

    if(alreadyInDB > 0) {
        return "Match already in database";
    }
    // if(Object.keys(home_statistics).length === 0) {
    //     return "Empty statistics";
    // }
    const [teamHome] = await Teams.findOrCreate({
        where: {
            sports_id: season.sports_id,
            leagues_id: season.leagues_id,
            name: home.name,
            abbreviation: home.abbreviation,
            espn_id: home.espn_id
        }
    });
    const [teamAway] = await Teams.findOrCreate({
        where: {
            sports_id: season.sports_id,
            leagues_id: season.leagues_id,
            name: away.name,
            abbreviation: away.abbreviation,
            espn_id: away.espn_id
        }
    });

    const [matchStatus] = await Statuses.findOrCreate({
        where: {
            name: status
        }
    });

    await Statistics.create({
        sports_id: season.sports_id,
        leagues_id: season.leagues_id,
        seasons_id: season.id,
        home_id: teamHome.get("id"),
        away_id: teamAway.get("id"),
        status_id: matchStatus.get("id"),
        ...match,
        ...home_statistics,
        ...away_statistics
    });

}