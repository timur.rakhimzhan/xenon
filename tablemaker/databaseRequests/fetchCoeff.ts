import axios, {AxiosResponse} from "axios";
import {findMatch, updateDate} from "../../services/oddsApi";
import {updateAbbreviations} from "./updateAbbreviation";
import moment from "moment";
import {CoeffsFetchedI} from "../../typings/tablemaker";
import {EspnCompetitorI, EspnScoreboardI} from "../../typings/tablemaker/espn";
import {Season} from "../../services/season";
import {CustomError, MORE_THAN_ONE_MATCH} from "../../services/customError";

export async function fetchCoeffs(season: Season, event: EspnScoreboardI, home: EspnCompetitorI, away: EspnCompetitorI): Promise<CoeffsFetchedI> {
    let coefficients: {rowCount: number, rows: Array<any>} = (await axios.get(findMatch(season), {
        params: {
            team_1: home.team.displayName,
            team_2: away.team.displayName,
            start_date: event.competitions[0].startDate,
            team_1_abbreviation: home.team.abbreviation,
            team_2_abbreviation: away.team.abbreviation,
            team_1_score: parseInt(home.score) > 0 ? parseInt(home.score) : null,
            team_2_score: parseInt(away.score) > 0 ? parseInt(away.score) : null,
        }
    })).data;

    let home_coeff: number = 0, away_coeff: number = 0;

    if(coefficients.rowCount === 1) {
        const match = coefficients.rows[0];
        let home_abbreviation = home.team.abbreviation, away_abbreviation = away.team.abbreviation;
        let home_id = match["TeamsHome"]["id"], away_id = match["TeamsAway"]["id"];
        home_coeff = match.home_coeff[match.home_coeff.length - 1];
        away_coeff = match.away_coeff[match.away_coeff.length - 1];

        if(match["TeamsHome"]["abbreviation"] === away.team.abbreviation ||  match["home_score"] === away.score) {
            [home_coeff, away_coeff] = [away_coeff, home_coeff];
            [home_abbreviation, away_abbreviation] = [away_abbreviation, home_abbreviation];
            [home_id, away_id] = [away_id, home_id];
        }

        updateAbbreviations(season, home_id, away_id, home_abbreviation, away_abbreviation)
            .catch((error) => console.log(`Error updating abbreviations`, error));

        if(!moment(match["start_date"]).isSame(event.competitions[0].startDate)) {
            axios.post(updateDate(season), {
                id: match.id,
                start_date: event.competitions[0].startDate
            }).catch((error) => console.log(`Error updating date`, error));
        }
    } else if(coefficients.rowCount > 1) {
        throw new CustomError(`2 matches from odds api event id: ${JSON.stringify(coefficients.rows)}`, MORE_THAN_ONE_MATCH);
    }
    return {home_coeff, away_coeff};
}