import {setupTable} from "./setupTable";
import {EspnCompetitorI, EspnScoreboardI, EspnMatchI} from "../../typings/tablemaker/espn";
import {StatisticsDBI} from "../../typings/tablemaker/matchesDB";
import  {Transaction, Sequelize} from "sequelize";
import {createMatchRecord} from "./createMatchRecord";
import {Season} from "../../services/season";
import {espnMatchById} from "../../services/espnApi";
import {getEventInfo} from "../../services/getEventInfo";
import {acceptableError, CustomError} from "../../services/customError";
import {STATUS_FINAL, STATUS_FULL_TIME} from "../../services/statuses";


export async function databaseRequests(season: Season, espnScoreboard: EspnScoreboardI, sequelize: Sequelize) {
    try {
        const statisticsDB: StatisticsDBI = await getEventInfo(season, espnScoreboard);
        if(statisticsDB.status === STATUS_FINAL || statisticsDB.status === STATUS_FULL_TIME) {
            await setupTable(season, statisticsDB, sequelize);
            return await createMatchRecord(season, statisticsDB);
        }
    } catch(error) {
        if(acceptableError(error)) {
            return;
        }
        throw error;
    }
}
