"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const oddsApi_1 = require("../../services/oddsApi");
const updateAbbreviation_1 = require("./updateAbbreviation");
const moment_1 = __importDefault(require("moment"));
const customError_1 = require("../../services/customError");
function fetchCoeffs(season, event, home, away) {
    return __awaiter(this, void 0, void 0, function* () {
        let coefficients = (yield axios_1.default.get(oddsApi_1.findMatch(season), {
            params: {
                team_1: home.team.displayName,
                team_2: away.team.displayName,
                start_date: event.competitions[0].startDate,
                team_1_abbreviation: home.team.abbreviation,
                team_2_abbreviation: away.team.abbreviation,
                team_1_score: parseInt(home.score) > 0 ? parseInt(home.score) : null,
                team_2_score: parseInt(away.score) > 0 ? parseInt(away.score) : null,
            }
        })).data;
        let home_coeff = 0, away_coeff = 0;
        if (coefficients.rowCount === 1) {
            const match = coefficients.rows[0];
            let home_abbreviation = home.team.abbreviation, away_abbreviation = away.team.abbreviation;
            let home_id = match["TeamsHome"]["id"], away_id = match["TeamsAway"]["id"];
            home_coeff = match.home_coeff[match.home_coeff.length - 1];
            away_coeff = match.away_coeff[match.away_coeff.length - 1];
            if (match["TeamsHome"]["abbreviation"] === away.team.abbreviation || match["home_score"] === away.score) {
                [home_coeff, away_coeff] = [away_coeff, home_coeff];
                [home_abbreviation, away_abbreviation] = [away_abbreviation, home_abbreviation];
                [home_id, away_id] = [away_id, home_id];
            }
            updateAbbreviation_1.updateAbbreviations(season, home_id, away_id, home_abbreviation, away_abbreviation)
                .catch((error) => console.log(`Error updating abbreviations`, error));
            if (!moment_1.default(match["start_date"]).isSame(event.competitions[0].startDate)) {
                axios_1.default.post(oddsApi_1.updateDate(season), {
                    id: match.id,
                    start_date: event.competitions[0].startDate
                }).catch((error) => console.log(`Error updating date`, error));
            }
        }
        else if (coefficients.rowCount > 1) {
            throw new customError_1.CustomError(`2 matches from odds api event id: ${JSON.stringify(coefficients.rows)}`, customError_1.MORE_THAN_ONE_MATCH);
        }
        return { home_coeff, away_coeff };
    });
}
exports.fetchCoeffs = fetchCoeffs;
//# sourceMappingURL=fetchCoeff.js.map