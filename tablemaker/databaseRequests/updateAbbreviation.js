"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const oddsApi_1 = require("../../services/oddsApi");
function updateAbbreviations(season, teams_id_1, teams_id_2, teams_abbrev_1, teams_abbrev_2) {
    const promises = [
        axios_1.default.post(oddsApi_1.updateAbbrev(season), {
            id: teams_id_1,
            abbreviation: teams_abbrev_1
        }),
        axios_1.default.post(oddsApi_1.updateAbbrev(season), {
            id: teams_id_2,
            abbreviation: teams_abbrev_2
        })
    ];
    return Promise.all(promises);
}
exports.updateAbbreviations = updateAbbreviations;
//# sourceMappingURL=updateAbbreviation.js.map