import axios from "axios";
import {updateAbbrev} from "../../services/oddsApi";
import {Season} from "../../services/season";

export function updateAbbreviations(season: Season, teams_id_1: number, teams_id_2: number, teams_abbrev_1: string, teams_abbrev_2: string) {
    const promises = [
        axios.post(updateAbbrev(season), {
            id: teams_id_1,
            abbreviation: teams_abbrev_1
        }),
        axios.post(updateAbbrev(season), {
            id: teams_id_2,
            abbreviation: teams_abbrev_2
        })
    ];
    return Promise.all(promises);
}