import store from "../../state/store";
import {setStatsAwayAction, setStatsHomeAction} from "../../state/actions/tablemaker";
import {StatisticsDBI} from "../../typings/tablemaker/matchesDB";
import {State} from "../../typings/states";
import {initStatistics, Statistics} from "../../database/models/statistics";
import {Sequelize} from "sequelize";
import {Season} from "../../services/season";

export async function setupTable(season: Season, statisticsDB: StatisticsDBI, sequelize: Sequelize): Promise<void> {
    const {tablemaker}: State = store.getState();
    const {home_statistics, away_statistics} = statisticsDB;

    if(!tablemaker.statisticsColumnsHome || !tablemaker.statisticsColumnsAway) {
        store.dispatch(setStatsHomeAction(new Set(Object.keys(home_statistics))));
        store.dispatch(setStatsAwayAction(new Set(Object.keys(away_statistics))));
        return await initStatistics(statisticsDB, season.matchesTableName, sequelize);
    }

    // const statisticsColumnsHome = new Set(Object.keys(home_statistics));
    // const statisticsColumnsAway = new Set(Object.keys(away_statistics));
    // const differenceHome = setDifference(statisticsColumnsHome, tablemaker.statisticsColumnsHome);
    // const differenceAway = setDifference(statisticsColumnsAway, tablemaker.statisticsColumnsAway);
    //
    // if(differenceHome.size || differenceAway.size) {
    //     store.dispatch(setStatsHomeAction(new Set([...statisticsColumnsHome, ...tablemaker.statisticsColumnsHome])));
    //     store.dispatch(setStatsAwayAction(new Set([...statisticsColumnsAway, ...tablemaker.statisticsColumnsAway])));
    //     return await initStatistics(statisticsDB, season.matchesTableName, sequelize);
    // }
}