"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const setupTable_1 = require("./setupTable");
const createMatchRecord_1 = require("./createMatchRecord");
const getEventInfo_1 = require("../../services/getEventInfo");
const customError_1 = require("../../services/customError");
const statuses_1 = require("../../services/statuses");
function databaseRequests(season, espnScoreboard, sequelize) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const statisticsDB = yield getEventInfo_1.getEventInfo(season, espnScoreboard);
            if (statisticsDB.status === statuses_1.STATUS_FINAL || statisticsDB.status === statuses_1.STATUS_FULL_TIME) {
                yield setupTable_1.setupTable(season, statisticsDB, sequelize);
                return yield createMatchRecord_1.createMatchRecord(season, statisticsDB);
            }
        }
        catch (error) {
            if (customError_1.acceptableError(error)) {
                return;
            }
            throw error;
        }
    });
}
exports.databaseRequests = databaseRequests;
//# sourceMappingURL=index.js.map