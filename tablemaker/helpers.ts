import {AxiosError} from "axios";
import {PickCenterDBI, TeamStatsI} from "../typings/tablemaker/matchesDB";
import {EspnMatchI} from "../typings/tablemaker/espn";
import {PickCenterFetchesI, PredictionsFetchesI} from "../typings/tablemaker";
import store from "../state/store";
import {State} from "../typings/states";

export function statisticsProcessor(statistics_arr: any, prefix: string): TeamStatsI {
    return statistics_arr.reduce((accumulator: TeamStatsI, currentValue: any) => {
        let propertyName: string = currentValue.abbreviation || currentValue.label;
        if(!propertyName) {
            return accumulator;
        }
        propertyName = propertyName.split(" ").join("_").toLowerCase();
        propertyName = propertyName.split("%").join("_percent");
        if(currentValue.displayValue.includes("-") && parseInt(currentValue.displayValue) >= 0) {
            const [total, attempts] = currentValue.displayValue.split("-");
            setStatisticProperty(accumulator, prefix + "_" + propertyName + "_attempted", parseFloat(attempts));
            setStatisticProperty(accumulator, prefix + "_" + propertyName + "_made", parseFloat(total));
        } else {
            setStatisticProperty(accumulator, prefix + "_" + propertyName, parseFloat(currentValue.displayValue));
        }
        return accumulator;
    }, {});
}


function setStatisticProperty(object:TeamStatsI, property: string, value: number) {
    const {tablemaker}: State = store.getState();

    if(!tablemaker.statisticsColumnsAway || !tablemaker.statisticsColumnsHome) {
        object[property] = value;
    } else if(tablemaker.statisticsColumnsHome.has(property) || tablemaker.statisticsColumnsAway.has(property)) {
        object[property] = value;
    }

    return object;
}

export function repeatHOF(func: Function, ...args: Array<any>): Promise<any> {
    return new Promise((resolve, reject) => {
        func(...args).then((res: any) => resolve(res))
            .catch((error:any) => setTimeout(() => {
                console.log(error);
                repeatHOF(func, ...args)
                    .then(() => resolve());
            }, 1000));
    })
}

export function setDifference<T>(firstTerm: Set<T>, secondTerm: Set<T>): Set<T> {
    const difference: Set<T> = new Set(firstTerm);
    for(const value of secondTerm.values()) {
        difference.delete(value);
    }
    return difference;
}


export function getEspnPrediction(match_espn: EspnMatchI): PredictionsFetchesI {
    let home_prediction = null, away_prediction = null;
    if(match_espn?.predictor?.homeTeam?.gameProjection) {
        home_prediction = parseFloat(match_espn.predictor.homeTeam.gameProjection);
        away_prediction = 100 - parseFloat(match_espn.predictor.homeTeam.gameProjection);
    }
    if(match_espn?.predictor?.awayTeam?.gameProjection) {
        away_prediction = parseFloat(match_espn.predictor.awayTeam.gameProjection);
        home_prediction = 100 - parseFloat(match_espn.predictor.awayTeam.gameProjection);
    }
    return {home_prediction, away_prediction};
}

export function getPickCenter(match_espn: EspnMatchI): PickCenterFetchesI{
    let home_pickcenter: Array<PickCenterDBI> | null = null, away_pickcenter: Array<PickCenterDBI> | null = null;
    if(match_espn.pickcenter) {
        home_pickcenter = match_espn.pickcenter
            .filter((pickcenter) => pickcenter.awayTeamOdds.winPercentage || pickcenter.homeTeamOdds.winPercentage)
            .map((pickcenter) => {
                let provider = pickcenter.provider.name;
                let winPercentage = -1;
                if(pickcenter.homeTeamOdds.winPercentage) {
                    winPercentage = pickcenter.homeTeamOdds.winPercentage;
                } else if(pickcenter.awayTeamOdds.winPercentage) {
                    winPercentage = pickcenter.awayTeamOdds.winPercentage
                }
                return {provider, winPercentage};
            });


        away_pickcenter = match_espn.pickcenter
            .filter((pickcenter) => pickcenter.awayTeamOdds.winPercentage || pickcenter.homeTeamOdds.winPercentage)
            .map((pickcenter) => {
                let provider = pickcenter.provider.name;
                let winPercentage = -1;
                if(pickcenter.awayTeamOdds.winPercentage) {
                    winPercentage = pickcenter.awayTeamOdds.winPercentage;
                } else if(pickcenter.homeTeamOdds.winPercentage) {
                    winPercentage = pickcenter.homeTeamOdds.winPercentage
                }
                return {provider, winPercentage};
            })
    }
    return {away_pickcenter, home_pickcenter};
}