"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const store_1 = __importDefault(require("../state/store"));
function statisticsProcessor(statistics_arr, prefix) {
    return statistics_arr.reduce((accumulator, currentValue) => {
        let propertyName = currentValue.abbreviation || currentValue.label;
        if (!propertyName) {
            return accumulator;
        }
        propertyName = propertyName.split(" ").join("_").toLowerCase();
        propertyName = propertyName.split("%").join("_percent");
        if (currentValue.displayValue.includes("-") && parseInt(currentValue.displayValue) >= 0) {
            const [total, attempts] = currentValue.displayValue.split("-");
            setStatisticProperty(accumulator, prefix + "_" + propertyName + "_attempted", parseFloat(attempts));
            setStatisticProperty(accumulator, prefix + "_" + propertyName + "_made", parseFloat(total));
        }
        else {
            setStatisticProperty(accumulator, prefix + "_" + propertyName, parseFloat(currentValue.displayValue));
        }
        return accumulator;
    }, {});
}
exports.statisticsProcessor = statisticsProcessor;
function setStatisticProperty(object, property, value) {
    const { tablemaker } = store_1.default.getState();
    if (!tablemaker.statisticsColumnsAway || !tablemaker.statisticsColumnsHome) {
        object[property] = value;
    }
    else if (tablemaker.statisticsColumnsHome.has(property) || tablemaker.statisticsColumnsAway.has(property)) {
        object[property] = value;
    }
    return object;
}
function repeatHOF(func, ...args) {
    return new Promise((resolve, reject) => {
        func(...args).then((res) => resolve(res))
            .catch((error) => setTimeout(() => {
            console.log(error);
            repeatHOF(func, ...args)
                .then(() => resolve());
        }, 1000));
    });
}
exports.repeatHOF = repeatHOF;
function setDifference(firstTerm, secondTerm) {
    const difference = new Set(firstTerm);
    for (const value of secondTerm.values()) {
        difference.delete(value);
    }
    return difference;
}
exports.setDifference = setDifference;
function getEspnPrediction(match_espn) {
    var _a, _b, _c, _d, _e, _f;
    let home_prediction = null, away_prediction = null;
    if ((_c = (_b = (_a = match_espn) === null || _a === void 0 ? void 0 : _a.predictor) === null || _b === void 0 ? void 0 : _b.homeTeam) === null || _c === void 0 ? void 0 : _c.gameProjection) {
        home_prediction = parseFloat(match_espn.predictor.homeTeam.gameProjection);
        away_prediction = 100 - parseFloat(match_espn.predictor.homeTeam.gameProjection);
    }
    if ((_f = (_e = (_d = match_espn) === null || _d === void 0 ? void 0 : _d.predictor) === null || _e === void 0 ? void 0 : _e.awayTeam) === null || _f === void 0 ? void 0 : _f.gameProjection) {
        away_prediction = parseFloat(match_espn.predictor.awayTeam.gameProjection);
        home_prediction = 100 - parseFloat(match_espn.predictor.awayTeam.gameProjection);
    }
    return { home_prediction, away_prediction };
}
exports.getEspnPrediction = getEspnPrediction;
function getPickCenter(match_espn) {
    let home_pickcenter = null, away_pickcenter = null;
    if (match_espn.pickcenter) {
        home_pickcenter = match_espn.pickcenter
            .filter((pickcenter) => pickcenter.awayTeamOdds.winPercentage || pickcenter.homeTeamOdds.winPercentage)
            .map((pickcenter) => {
            let provider = pickcenter.provider.name;
            let winPercentage = -1;
            if (pickcenter.homeTeamOdds.winPercentage) {
                winPercentage = pickcenter.homeTeamOdds.winPercentage;
            }
            else if (pickcenter.awayTeamOdds.winPercentage) {
                winPercentage = pickcenter.awayTeamOdds.winPercentage;
            }
            return { provider, winPercentage };
        });
        away_pickcenter = match_espn.pickcenter
            .filter((pickcenter) => pickcenter.awayTeamOdds.winPercentage || pickcenter.homeTeamOdds.winPercentage)
            .map((pickcenter) => {
            let provider = pickcenter.provider.name;
            let winPercentage = -1;
            if (pickcenter.awayTeamOdds.winPercentage) {
                winPercentage = pickcenter.awayTeamOdds.winPercentage;
            }
            else if (pickcenter.homeTeamOdds.winPercentage) {
                winPercentage = pickcenter.homeTeamOdds.winPercentage;
            }
            return { provider, winPercentage };
        });
    }
    return { away_pickcenter, home_pickcenter };
}
exports.getPickCenter = getPickCenter;
//# sourceMappingURL=helpers.js.map