export interface TeamStatsI {
    [key: string]: number
}

export interface TeamInfoDBI {
    abbreviation: string,
    name: string,
    espn_id: number,
    other_name?: Array<string>,

    [key: string]: any,
}

export interface MatchInfoDBI {
    home_score: number | null,
    home_coeff: number | null,
    home_prediction: number | null,
    home_pickcenter: Array<PickCenterDBI> | null,
    away_score: number | null,
    away_coeff: number | null,
    away_prediction: number | null,
    away_pickcenter: Array<PickCenterDBI> | null
    neutral_site: boolean,
    espn_id: number,
    overtime: boolean,
    periods: number,
    clock: number,
    start_date: string,
    [key: string]: any,
}


export interface MatchDBI {
    match: MatchInfoDBI,
    home: TeamInfoDBI,
    away: TeamInfoDBI,
    status: string,
}


export interface StatisticsDBI extends MatchDBI{
    home_statistics: TeamStatsI,
    away_statistics: TeamStatsI,
}

export interface PickCenterDBI {
    winPercentage: number,
    provider: string
}