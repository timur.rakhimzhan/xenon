export interface EspnCompetitorI {
    score: string,
    team: EspnTeamI,
    homeAway: string,
    statistics: Array<any>
}

export interface EspnTeamI {
    id: string,
    abbreviation: string,
    displayName: string,

    [key: string]: string | number,
}

export interface EspnStatusTypeI {
    completed: boolean,
    altDetail?: string,
    detail: string,
    name: string,
    state: string,
    shortDetail: string,
}

export interface EspnStatusI {
    clock: number,
    displayClock: string,
    period: number,
    type: EspnStatusTypeI
}

export interface EspnCompetitionI {
    attendance: number,
    date: string,
    competitors: Array<EspnCompetitorI>
    neutralSite: boolean,
    id: string,
    timeValid: boolean,
    status: EspnStatusI,
    startDate: string
}

export interface EspnScoreboardI {
    competitions: Array<EspnCompetitionI>,
    status: EspnStatusI,
    shortName: string,
    name: string,
    date: string,
    id: string
}

export interface EspnMatchI {
    predictor?: any,
    boxscore: EspnBoxScoreI,
    header: {
        status: {
            type: EspnStatusTypeI,
        },
        competitions: Array<EspnCompetitionI>
    }
    pickcenter?: Array<{
        homeTeamOdds: {
            winPercentage?: number
        },
        awayTeamOdds: {
            winPercentage?: number
        },
        provider: {
            name: string
        }
    }>
}

interface EspnBoxScoreI {
    teams: Array<EspnCompetitorI>,
    statistics: Array<any>
}