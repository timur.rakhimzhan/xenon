import {PickCenterDBI} from "./matchesDB";

export interface CoeffsFetchedI {
    home_coeff: number,
    away_coeff: number
}

export interface PredictionsFetchesI {
    home_prediction: number | null,
    away_prediction: number | null,
}

export interface PickCenterFetchesI {
    home_pickcenter: Array<PickCenterDBI> | null,
    away_pickcenter: Array<PickCenterDBI> | null
}
