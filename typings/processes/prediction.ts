export interface PredictionI {
    bet: string,
    home_probability?: number,
    away_probability?: number,
}

export interface PredictionInfoI extends PredictionI{
    home: string,
    homeAbbrev: string,
    homeCoeff: number,
    away: string,
    awayCoeff: number,
    awayAbbrev: string
    startDate: string,
    sportName: string,
    leagueName: string
}

export interface PredictionResultI extends PredictionInfoI {
    homeScore: number,
    awayScore: number,
    result: string
}