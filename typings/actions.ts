import {Action} from "redux";
import {FetcherState, TablemakerState} from "./states";

export interface TablemakerAction extends Action{
    payload: TablemakerState
}

export interface FetcherAction extends Action {
    payload: FetcherState
}