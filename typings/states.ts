export interface TablemakerState {
    statisticsColumnsHome?: Set<string>,
    statisticsColumnsAway?: Set<string>,
    tableAssociationsEstablished?: boolean
    tableCreated?: boolean
}

export interface FetcherState {
    daemon?: boolean;
}

export interface State {
    tablemaker: TablemakerState,
    fetcher: FetcherState
}